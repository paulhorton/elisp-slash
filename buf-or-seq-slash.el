;;; buf-or-seq-slash.el --- functions acting on a buffer or sequence  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241006
;; Updated: 20241006
;; Version: 0.0
;; Keywords:

;;; Commentary:

;; Some elisp builtin commands handle objects which can be buffers or strings.
;; for example, text property handling functions such as `get-text-property'

;; That suggests it might be useful to define a meta type including both buffers and strings.
;; For that see buf-or-string-slash.el
;;
;; It may also turn out to be useful to define a meta type which includes both buffers
;; and all sequence types － thus this file.

;; On the other hand, it may not actually be useful.
;; One reason is that in contrast to the union of buffer and string types only,
;; when including any sequence type, nil cannot be used as an out-of-band value.

;; whereas in the buf-or-string meta type case it can serve to indicate the current buffer.

;; In any case, I leave file around as a stub for now. PH20241008




;;; Change Log:

;;; Code:


;; No code yet.



(provide 'buf-or-seq-slash)

;;; buf-or-seq-slash.el ends here
