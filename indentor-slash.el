;;; indentor-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231204
;; Updated: 20231204
;; Version: 0.0
;; Keywords: indentation, 字下げ, 縮排

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'let1)



(defun indentor/1121 (pos parser-state)
  "Indentor function with indent 1,1,2,1,1,...
at those number of sexps from the top."
  (indentor '[1 1 2 1] pos parser-state)
  )

(defun indentor/11121 (pos parser-state)
  "Indentor function with indent 1,1,1,2,1,1,...
at those number of sexps from the top."
  (indentor '[1 1 1 2 1] pos parser-state)
  )

(defun indentor/11231 (pos parser-state)
  "Indentor function with indent 1,1,2,3,1,1,...
at those number of sexps from the top."
  (indentor '[1 1 2 3 1] pos parser-state)
  )

(defun indentor/22231 (pos parser-state)
  "Indentor function with indent 2,2,2,3,1,1,...
at those number of sexps from the top."
  (indentor '[2 2 2 3 1] pos parser-state)
  )
(defun indentor/111231 (pos parser-state)
  "Indentor function with indent 1,1,1,2,3,1,1,...
at those number of sexps from the top."
  (indentor '[1 1 1 2 3 1] pos parser-state)
  )

(defun indentor/111321 (pos parser-state)
  "Indentor function with indent 1,1,1,3,2,1,1,...
at those number of sexps from the top."
  (indentor '[1 1 1 3 2 1] pos parser-state)
  )



(defun indentor (indent-levels pos parser-state)
  "Return number of columns to indent;
according to INDENT-LEVELS and current position POS
and PARSER-STATE."
  (ignore pos); Silence compiler warning about pos not being used.
  (cl-check-type indent-levels sequence)
    (let (
          (levels-fin  (1- (length indent-levels)))
          (beg   (car (last (nth 9 parser-state))))
          (end              (nth 2 parser-state) )
          )
      (cl-assert (<= 0 levels-fin) t "if-let1/indentor: empty indent-levels")
      (let  (
             (base-column  (save-excursion
                             (goto-char beg)
                             (current-column)
                             ))
             (count  (indentor/how-many-sexps beg end))
             )
      (+ base-column (* lisp-body-indent
                        (elt indent-levels (min count levels-fin))
                        )))))


(defun indentor/how-many-sexps (beg end)
  "Number of sexps found up to position BEG, when looking back from position END."
  (save-excursion
    (let1  count  0
      (goto-char end)
      (while (and (< beg (point))
                  (condition-case
                      nil
                      (progn (forward-sexp -1) (setq count (1+ count)))
                    (scan-error nil)
                    )))
      count
      )))



(provide 'indentor-slash)

;;; indentor-slash.el ends here
