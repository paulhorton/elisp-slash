;;; seq-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231012
;; Updated: 20231012
;; Version: 0.0
;; Keywords: elisp sequence

;;; Commentary:

;;; To consider:
;;   use cl-defmethod, cl-defgeneric mechanism in the way that seq.el does
;;   that might make it easier to add new "sequence" types.

;;   Currently I use the low tech method of splitting seq/ namespace functions
;;   into two files
;;
;;   The dependences are:
;;   1. seq-slash-base-include
;;   2. list-slash, vec-slash
;;   3. seq-slash

;;; Change Log:

;;; Code:


(require 'seq-slash-base-include)
(require 'list-slash)
(require 'array-slash)


(cl-defun seq/bufinsert (seq &key buf (format-spec "%s") (delim "\t") pre post)
  "Insert sequence SEQ according to FORMAT-SPEC;
DELIM is inserted in between elements.
when given, PRE and POST are inserted before or after the list.
If their value is t, then DELIM is inserted instead.
Insertion is done in buffer BUF defaulting to the current buffer."
      (cond
       ((listp seq)
        (list/bufinsert seq :buf buf :format-spec format-spec :delim delim :pre pre :post post)
        )
       ((arrayp seq)
        (array/bufinsert seq :buf buf :format-spec format-spec :delim delim :pre pre :post post)
        )
       (
        (error "/home/paulh/gitlab/elisp-slash/ SEQ was neither list nor array")
        )))


(defun seq/coerce-to-same-type (seq-to-coerce seq-of-desired-type)
  "Return SEQ-TO-COERCE as type SEQ-OF-DESIRED-TYPE
If their types are the same SEQ-TO-COERCE itself is returned.

Bool-vector and Char-table types are not currently supported."
  (cl-coerce
   seq-to-coerce
   (cl-etypecase seq-of-desired-type
    (vector 'vector)
    (string 'string)
    (list   'list)
    )))



;; ────────────────────  filtering  ────────────────────

;; New 20240502 still experimenting with naming,
;; argument orger and how much to generalize.
;;
;; In the small, we could do (limit seq &rest more-seqs)
;;
;; In the large, we could have
;; (seq/list-keys< (limit key-fun seq)
;;
;; and/or
;;
;; (seq/list-vals-true-pred2 2ary-predicate arg seq)
;; Filtering values of SEQ via
;;   (funcall 2ary-predicate arg value)
;;
(defun seq/list-vals< (limit seq)
  "Return list of values in SEQ"
  (let (acc)
    (mapc
     (lambda (elem)
         (when (< elem limit)
           (push elem acc)
           ))
       seq
       )
    (reverse acc)
     ))



;; ────────────────────  length comparision  ────────────────────

(defun seq/len< (seq1 seq2)
  "Iff seq SEQ2 is longer than seq SEQ1, return true."
  (cl-check-type seq1 sequence)
  (cl-check-type seq2 sequence)
  (if (listp seq1)
      (if (listp seq2)
          (list/len< seq1 seq2)
        ;; else seq1 is a list, but seq2 not
        (length< seq1 (length seq2))
        )
    ;; else seq1 is not a list
    (not (or (length< seq2 (length seq1))
             (length= seq2 (length seq1))
             ))))


(defun seq/len≦ (seq1 seq2)
  "Iff seq SEQ2 as long as seq SEQ1, return true."
  (cl-check-type seq1 sequence)
  (cl-check-type seq2 sequence)
  (if (listp seq1)
      (if (listp seq2)
          (list/len≦ seq1 seq2)
        ;; else seq1 is a list, but seq2 not
        (or
         (length< seq1 (length seq2))
         (length= seq1 (length seq2))
         ))
    ;; else seq1 is not a list
    (not (length< seq2 (length seq1)))
    ))


(defun seq/len= (seq1 seq2)
  "Iff seqs SEQ1 and SEQ2 have equal length, return true."
  (cl-check-type seq1 sequence)
  (cl-check-type seq2 sequence)
  (if (listp seq1)
      (if (listp seq2)
          (list/len= seq1 seq2)
        ;; else seq1 is a list, but seq2 not
        (length= seq1 (length seq2))
        )
    ;; else seq1 is not a list
    (length= seq2 (length seq1))
    ))




;; ────────────────────  numerics  ────────────────────

(cl-defun seq/*scalar (seq scalar &key (trash 'keep))
  "Like `seq/*scalar/list', but return type matches that of SEQ"
  (seq/coerce-to-same-type
   (seq/*scalar/list seq scalar :trash trash)
   seq
   ))

(cl-defun seq/*scalar/list (seq scalar &key (trash 'keep))
  "Return multiplication of SEQ with SCALAR.
SCALAR should be a number.
Returned sequence is always a list.

TRASH determines the behavior when a non-numerical element is
encountered.
  TRASH     action
  'keep     Include in returned seq
  'drop     Quietly ignore
  nil       Throw error
"
  (cl-check-type seq sequence)
  (cl-check-type scalar number)
  (let1 res
      (mapcar
       (lambda (elem)
         (if (numberp elem)
             (* scalar elem)
           (cl-case trash
             (keep elem)
             (drop nil); to remove later
             (t   (error "non-numeric element:  %S" elem))
             )))
       seq
       )
    (if (eq trash 'drop)
        (delete nil res)
      res
      )))


;; ────────────────────  seq/do macros  ────────────────────

(defmacro seq/do-idx (seq elem-var idx-spec &rest body)
  "Special case of `seq/do-tween-idx' with no TWEEN."
  (declare
   (indent indentor/1121)
   (debug (form symbolp (symbolp &optional form form) body))
   )
  (or (symbolp elem-var)  (error "seq/do-idx elem-var should be a symbol"))
  (or (listp   idx-spec)  (error "seq/do-idx idx-spec should be a list"))
  (or body (error "seq/do-idx empty body"))
  `(if (listp ,seq)
       (list/do-idx ,seq ,elem-var ,idx-spec ,@body)
     (array/do-idx ,seq ,elem-var ,idx-spec ,@body)
     ))


(defmacro seq/do-idx* (seq elem-var idx-spec result &rest body)
  "Special case of `seq/do-tween-idx*' with no TWEEN"
  (declare
   (indent indentor/1121)
   (debug (form symbolp (symbolp &optional form form) form body))
   )
  (or (symbolp elem-var) (error "list/do-idx* elem-var should be a symbol"))
  (or body (error "list/do-idx empty body"))
  `(if (listp ,seq)
       (list/do-tween-idx* ,seq ,elem-var ,idx-spec ,result nil ,@body)
     (array/do-tween-idx* ,seq ,elem-var ,idx-spec ,result nil ,@body)
     ))


(defmacro seq/do-tween (seq elem-var tween &rest body)
  "Evalues body for each element of sequence SEQ,
evaluate TWEEN between iterations

Simplified, special case `seq/do-tween-idx*'
\(fn SEQ ELEM-VAR RESULT TWEEN BODY)"
  (declare
   (indent indentor/1121)
   (debug (form symbolp form body))
   )
  (or (symbolp elem-var) (error "seq/do-tween elem-var should be a symbol"))
  (or body (error "seq/do-tween empty body"))
  `(if (listp ,seq)
       (list/do-tween* ,seq ,elem-var nil ,tween ,@body)
     (array/do-tween* ,seq ,elem-var nil ,tween ,@body)
     ))


(defmacro seq/do-tween* (seq elem-var result tween &rest body)
  "Evalues body for each element of sequence SEQ,
evaluate TWEEN between iterations

Simplified, special case `seq/do-tween-idx*'
\(fn SEQ ELEM-VAR RESULT TWEEN BODY)"
  (declare
   (indent indentor/11231)
   (debug (form symbolp form form body))
   )
  (or (symbolp elem-var) (error "seq/do-tween* elem-var should be a symbol"))
  (or body (error "seq/do-tween* empty body"))
  `(if (listp ,seq)
       (list/do-tween* ,seq ,elem-var ,result ,tween ,@body)
     (array/do-tween* ,seq ,elem-var ,result ,tween ,@body)
     ))


(defmacro seq/do-tween-idx (seq elem-var idx-spec tween &rest body)
  "Like `seq/do-tween-idx*', but normally returns nil

\(fn SEQ ELEM-VAR (IDX-VAR [STEP [START [LIMIT]]]) TWEEN BODY)"
  (declare
   (indent indentor/11121)
   (debug (form symbolp (symbolp &optional form form) form body))
   )
  (or  (symbolp elem-var)
       (error "seq/do-tween-idx elem-var should be a symbol but received: %S" elem-var))
  (and (symbolp tween)
       (error "seq/do-tween-idx, does not make sense for TWEEN to be a symbol, but received: %S" tween))
  (or body (error "seq/do-tween-idx empty body"))
  `(if (listp ,seq)
       (list/do-tween-idx* ,seq ,elem-var ,idx-spec nil ,tween ,@body)
     (array/do-tween-idx* ,seq ,elem-var ,idx-spec nil ,tween ,@body)
     ))


(defmacro seq/do-tween-idx* (seq elem-var idx-spec result tween &rest body)
  ;; TO CONSIDER: split between macros which can or cannot modify list seq via ELEM-VAR
  ;; perhaps seq/do-tween-idx*! for the one which can modify.
  "Evaluate BODY for indices of list L, with element value
bound to ELEM-VAR its index bound to IDX-VAR.

TWEEN should be a form to be executed between iterations
of the loop. The binding ELEM-VAR is not visible to code
in TWEEN.

Negative values of START and LIMIT are treated as counting
from the end of the list;
i.e. -1 for the final element of L, -2 for the penultimate one...

In the loop, ELEM-VAR is generalized variable;
so the contents of SEQ can be changed by setting ELEM-VAR

\(fn SEQ ELEM-VAR (IDX-VAR [STEP [START [LIMIT]]]) RESULT TWEEN BODY)"
  (declare
   (indent indentor/111321)
   (debug (form symbolp (symbolp &optional form form) form form body))
   )
  (or (symbolp elem-var) (error "seq/do-tween-idx* elem-var should be a symbol"))
  (or body (error "seq/do-tween-idx* empty body"))
  `(if (listp ,seq)
       (list/do-tween-idx* ,seq ,elem-var ,idx-spec ,result ,tween ,@body)
     (array/do-tween-idx* ,seq ,elem-var ,idx-spec ,result ,tween ,@body)
     ))



(provide 'seq-slash)

;;; seq-slash.el ends here
