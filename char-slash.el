;;; char-slash.el --- character related functions  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241212
;; Updated: 20241212
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'shortcuts)


;; Sometimes it can be useful have an out-of-band value to represent that a value is missing.
;; In Perl undef plays that role, in Lisp sometimes nil can.
;;
;; Fixme?  The name `char/equal?/nilOK' is self-descriptive, but perhaps a bit awkward?
(defun char/equal?/nilOK (arg1 arg2)
  "Similar `char-equal',
but if either arg is nil, quietly return nil.
When true, return ARG1"
  ;; To Consider: `=' like arg specification (char/equal/nilOK arg1 &rest more args)
  (and arg1 arg2 (char-equal arg1 arg2) arg1)
  )


(defvar char/width/alist
  '(
    (?⭾ . 2)
    )
  "Associate characters with a guess at their approximate width when
displayed with a non-proportional font."
  )


(cl-defun char/width (C &optional tabchar-width)
  "Return a guess at the width of character C;
in a \"standard\" proportional font.

For most characters C, this just forwards the value of `char-width'.
but for a few characters a different width is returned.

In those cases the returned width better reflects the displayed width
on a graphical display (in my debian environment, PH2025).
"
  ;; todo: Figure out how to get a precise width for non-proportional fonts
  (if (display-graphic-p)
      (or  (cdrassoc C char/width/alist)
           (and (= C #x9) tabchar-width)
           (char-width C)
           )
    (char-width C)
    ))



(provide 'char-slash)

;;; char-slash.el ends here
