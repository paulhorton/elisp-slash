;;; regex-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231106
;; Updated: 20231106
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'let1)
(require 'shortcuts); maybe better not to be dependent on this...


;; Afterthought: 20240530
;; Conditionally binding inhibit-changing-match-data might
;; be a better approach than using save-match-data.
;;
;; For one thing, apparently save-match-data does
;; unwind-protect, which may not be necessary.
;; see https://mbork.pl/2018-12-03_looking-back-p
(defmacro regex/maybe-save-match-data (really? &rest body)
  "Evaluate body, maybe wrapped in `save-match-data'
But not so when REALLY? is nil."
  (declare
   (indent 1)
   (debug (symbolp body))
   )
  ;; Guessing it may help compiler optimization,
  ;; I separate cases where really? is a literal nil or t.
  ;; TODO: figure out if it does, and if not simplify.
  (cond
   ((eq really? nil)
    `(progn ,@body)
    )
   ((eq really? t)
    `(save-match-data
      (progn ,@body)
      )
    )
   (t
    `(if ,really?
         (save-match-data
           (progn ,@body)
           )
       (progn ,@body)
       ))
   ))


(defmacro regex/maybe-with-case-table (case-table &rest body)
  "Wrap BODY in:
`with-case-table'          If CASE-TABLE is a case table
f(let1 case-fold-search t)  If CASE-TABLE is nil
Just `progn'               Otherwise"
  (declare
   (indent 1)
   (debug (symbolp body))
   )
  ;; Guessing it may help compiler optimization,
  ;; I separate cases where really? is a literal nil or t.
  ;; TODO: figure out if it does, and if not simplify.
  (cond
   ((eq case-table nil)
    `(let1 case-fold-search t
       ,@body
       ))
   ((eq case-table t)
    `(progn ,@body)
    )
   (t
    `(if ,case-table
        (if (eq ,case-table t)
            (progn ,@body)
          ;; (cl-check-type ,case-table case-table)
          (with-case-table ,case-table
            ,@body
            ))
       (let1 case-fold-search t
         ,@body
         )))))

(defun regex/match (regex text &optional drop-props?)
  "If REGEX matches TEXT return the (whole) match,
dropping text properties when DROP-PROPS? is true.

Group matches can be extracted from the match data.
see also `s-match'"
  (regex/match/all-args regex text 0 drop-props? nil t)
  )

(defun regex/imatch (regex text &optional drop-props?)
  "Case insensitive version of `regex/match'"
  (regex/match/all-args regex text 0 drop-props? nil nil)
  )

(defun regex/cmatch (case-table regex text &optional drop-props?)
  "Like `regex/match', but temporarily using CASE-TABLE."
  (regex/match/all-args regex text 0 drop-props? nil case-table)
  )

(defun regex/match? (regex text &optional drop-props?)
  "Like `regex/match', but restores the match data.
see also `s-match'"
  (regex/match/all-args regex text 0 drop-props?  t  t)
  )

(defun regex/imatch? (regex text &optional drop-props?)
  "Case insensitive version of `regex/match?'"
  (regex/match/all-args regex text 0 drop-props?  t  nil)
  )

(defun regex/cmatch? (case-table regex text &optional drop-props?)
  "Like `regex/match?', but temporarily using CASE-TABLE."
  (regex/match/all-args regex text 0 drop-props?  t  case-table)
  )

(defun regex/match/from (regex text pos &optional drop-props?)
    "If REGEX matches TEXT at or following position POS,
return the match; but sans text properties if DROP-PROPS? is true."
  (regex/match/all-args regex text pos drop-props? nil t)
  )

(defun regex/imatch/from (regex text pos &optional drop-props?)
    "Case insensitive version of `regex/match/from'"
  (regex/match/all-args regex text pos drop-props? nil nil)
  )

(defun regex/cmatch/from (case-table regex text pos &optional drop-props?)
    "Like `regex/match/from', but temporarily using CASE-TABLE."
  (regex/match/all-args regex text pos drop-props? nil case-table)
  )

(defun regex/match?/from (regex text pos &optional drop-props?)
  "`regex/match/all-args' with RESTORE-MD? true"
  (regex/match/all-args regex text pos drop-props?  t  t)
  )

(defun regex/imatch?/from (regex text pos &optional drop-props?)
  "Case insensitive version of `regex/match?/from'"
  (regex/match/all-args regex text pos drop-props?  t  nil)
  )

(defun regex/cmatch?/from (case-table regex text pos &optional drop-props?)
  "Like `regex/match?/from', but temporarily using CASE-TABLE."
  (regex/match/all-args regex text pos drop-props?  t  case-table)
  )

(defun regex/match/all-args (regex text pos drop-props? restore-md? case-table)
  "This function is mainly intended for internal use to be called
by functions like `regex/match'. As such, its argument or
their order may be redefined in the future.

Return match of REGEX in TEXT or nil if no match.
If DROP-PROPS? return copy of match without text properties.

Position POS arg is forwarded to `string-match'

When RESTORE-MD? true, Match data is restored,
otherwise group matches can be extracted from the match data.

If CASE-TABLE is t, do nothing special;
if it is a case table use that table,
and if nil, match with case folded.

See also: `regex/match?', `s-match'"
  (cl-check-type regex string)
  (cl-check-type text string)
  (cl-check-type pos integer)
  (cl-check-type drop-props? boolean)
  (cl-check-type restore-md? boolean)
  (cl-check-type case-table (or boolean case-table))
  (regex/maybe-save-match-data restore-md?
    (regex/maybe-with-case-table case-table
      (when (string-match regex text pos)
        (substring* (match-string 0 text) nil nil drop-props?)
        ))))


(defun regex/match?/fun (regex &optional drop-props?)
   "Return unary function useful for filtering matches to REGEX"
   (lambda (text)
     (regex/match regex text drop-props?)
     ))


(defun regex/not-match (regex text &optional drop-props?)
  "If REGEX does not match TEXT, return TEXT;
dropping text properties when DROP-PROPS? is true.

Otherwise return nil. In which case the
match can be retrieved from the match data."
  (regex/not-match/all-args regex text 0 drop-props? nil t)
  )


(defun regex/not-match? (regex text &optional drop-props?)
  "Like `regex/not-match' but restores the match data."
  (regex/not-match/all-args regex text 0 drop-props?  t  t)
  )

(defun regex/not-match/all-args (regex text pos drop-props? restore-md? case-table)
  "If TEXT does not match REGEX return TEXT, otherwise nil.

If given, the POS arg is forwarded to `string-match-p'

If DROP-PROPS? return a copy without text properties.

When RESTORE-MD? true, Match data is restored,
otherwise group matches can be extracted from the match data."
  (regex/maybe-save-match-data restore-md?
    (regex/maybe-with-case-table case-table
      (unless (string-match regex text pos)
        (if drop-props?
            (substring-no-props text)
          text
          )))))

(defun regex/not-match?/fun (regex &optional drop-props?)
   "Return unary function useful for filtering matches to REGEX"
   (lambda (text)
     (regex/not-match/all-args regex text 0 drop-props?  t  t)
     ))



(provide 'regex-slash)

;;; regex-slash.el ends here
