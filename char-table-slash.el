;;; char-table-slash.el --- working with char-tables  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240516
;; Updated: 20240516
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'dotimes-star)



(defun char-table/map-each-char (fun char-table)
  ;; Order of arguments (fun char-table)
  ;; follows convention of lisp mapcar and related map functions.
  "List of results of calling function FUN on elements of CHAR-TABLE
CHAR-TABLE should be a char-table
FUN should be function accepted 2 args: (key val)
both of which will be bound to characters
during the mapping.

Like `map-char-table', but splits up ranges."
  (cl-check-type fun function)
  (cl-check-type char-table char-table)
  (reverse
   (char-table/map-each-char/reversed fun char-table)
   ))


(defun char-table/map-each-char/reversed (fun char-table)
  "Same as `char-table/map-each-char'
but in reverse order of `map-char-table'."
  (cl-check-type fun function)
  (cl-check-type char-table char-table)
  (let (acc)
    (map-char-table
     (lambda (key-or-range val)
       (if (characterp key-or-range)
           (push (funcall fun key-or-range val)
                 acc
                 )
         (let ((beg (car key-or-range))
               (fin (cdr key-or-range))
               )
           (dotimes* (key beg (1+ fin))
             (push (funcall fun key val)
                   acc
                   )
             ))))
     char-table)
    acc
    ))


;; Like `mapc' but for char-table
(defun char-table/call-on-each-char (fun char-table)
  "Call function FUN once for each entry in CHAR-TABLE.
FUN should take two args (CHAR VAL)"
  (map-char-table
   (lambda (key-or-range val)
     (if (characterp key-or-range)
         (funcall fun key-or-range val)
       (let ((beg (car key-or-range))
             (fin (cdr key-or-range))
             )
         (dotimes* (key beg (1+ fin))
           (funcall fun key val)
             ))))
   char-table
   ))



;; TODO: Define macro (char-table/do-each-char (spec &rest body))
;; I guess that way might be faster than `char-table/call-on-each-char'

(defun char-table-dump (char-table)
  "Return list reflecting the contents of CHAR-TABLE."
  (let (acc)
    (map-char-table
     (lambda (char-or-range val)
       (push (cons char-or-range val) acc)
       )
     char-table
     )
    acc
    ))




(defun char-table/as-alist (char-table)
  "Return alist representing contents of CHAR-TABLE."
  (char-table/map-each-char #'cons char-table)
  )



(provide 'char-table-slash)

;;; char-table-slash.el ends here
