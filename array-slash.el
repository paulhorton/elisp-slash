;;; array-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240427
;; Updated: 20240427
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'seq-slash-base-include)
(require 'shortcuts);;   for:  `:'


;; ──────────  Looping Macros  ──────────
(defmacro array/do-idx (array elem-var idx-spec &rest body)
  "Special case of `array/do-tween-idx' with no TWEEN"
  (declare
   (indent indentor/1121)
   (debug (form symbolp (symbolp &optional form form) body))
   )
  (cl-assertf (mutable-symbol? elem-var) "array/do-idx; elem-var should be a mutable symbol, but got:  %S" elem-var)
  (cl-assertf body                       "array/do-idx; empty body")
  (cl-assertf (listp idx-spec)           "array/do-idx; idx-spec should be a list, but got:  %S" idx-spec)
  `(array/do-tween-idx* ,array ,elem-var ,idx-spec nil nil ,@body)
  )


(defmacro array/do-idx* (array elem-var idx-spec result &rest body)
  "Special case of `array/do-tween-idx*' with no TWEEN"
  (declare
   (indent indentor/1121)
   (debug (form symbolp (symbolp &optional form form) form body))
   )
  (or (symbolp elem-var) (error "array/do-idx* elem-var should be a symbol"))
  (or body (error "array/do-idx empty body"))
  `(array/do-tween-idx* ,array ,elem-var ,idx-spec ,result nil ,@body)
  )


(defmacro array/do-tween (array elem-var tween &rest body)
  "Evalues body for each element of ARRAY
evaluate TWEEN between iterations.

Normally returns nil, unless `cl-return' call in BODY
is triggered and returns some value.

See also `array/do-tween*' and `array/do-tween-idx'"
  (declare
   (indent indentor/1121)
   (debug (form symbolp form body))
   )
  (cl-assertf (mutable-symbol? elem-var) "array/do-tween; elem-var should be a mutable symbol, but got:  %S" elem-var)
  (cl-assertf body                       "array/do-tween; empty body")
  `(array/do-tween* ,array ,elem-var nil ,tween ,@body)
  )


(defmacro array/do-tween* (array elem-var result tween &rest body)
  "Evalues body for each element of ARRAY,
evaluate TWEEN between iterations

Simplified, special case `array/do-tween-idx*'"
  (declare
   (indent indentor/11231)
   (debug (form symbolp form form body))
   )
  (cl-assertf (mutable-symbol? elem-var) "array/do-tween*; elem-var should be a mutable symbol, but got:  %S" elem-var)
  (cl-assertf body                       "array/do-tween*; empty body")
  (let (
        (idx-var   (gensym "array/do-tween*/idx"))
        (limit     (gensym "array/do-tween*/limit"))
        )
    (cl-assert (symbolp elem-var) nil (format "Expected (nth 0 idx-spec) to be a symbol but got %S" elem-var))
    `(let (
           (,idx-var  0)
           (,limit    (length ,array))
           )
       (or
        (catch '--cl-block-nil--;;  Maybe should use cl-block, or different symbol name?
          (while (< ,idx-var ,limit)
            (cl-symbol-macrolet ((,elem-var (aref ,array ,idx-var)))
              ,@body
              )
              (setq ,idx-var (1+ ,idx-var))
              (if (< ,idx-var ,limit)
                  ,tween
                (throw '--cl-block-nil-- nil)
                )
              ))
          ,result
          ))))



(defmacro array/do-tween-idx* (array elem-var idx-spec result tween &rest body)
  "Evaluate BODY for indices of ARRAY, with element value
bound to ELEM-VAR and its index bound to IDX-VAR.

TWEEN should be a form to be executed between iterations
of the loop. The binding ELEM-VAR is not visible to code
in TWEEN.

Negative values of START and LIMIT are treated as counting
from the end of the array;
i.e. -1 for the final element of ARRAY, -2 for the penultimate one...

In the loop, ELEM-VAR is generalized variable;
so the contents of ARRAY can be changed by setting ELEM-VAR

\(fn ARRAY ELEM-VAR (IDX-VAR [STEP [START [LIMIT]]]) RESULT TWEEN BODY)"
  (declare
   (indent indentor/111321)
   (debug (form symbolp (symbolp &optional form form) form form body))
   )
  (cl-assertf (mutable-symbol? elem-var) "array/do-tween-idx*; elem-var should be a mutable symbol, but got:  %S" elem-var)
  (cl-assertf body                       "array/do-tween-idx*; empty body")
  (cl-assertf idx-spec                   "array/do-tween-idx*; idx-spec should not be nil")
  (cl-assertf (listp idx-spec)           "array/do-tween-idx*; idx-spec should be a list, but got:  %S" idx-spec)
  (let (
        (idx-var      (nth 0 idx-spec))
        (step-exp     (nth 1 idx-spec))
        (start-exp    (nth 2 idx-spec))
        (limit-exp    (nth 3 idx-spec))
        (step      (gensym "array/do-tween-idx*/step"))
        (limit     (gensym "array/do-tween-idx*/limit"))
        (len       (gensym "array/do-tween-idx*/len"))
        )
    `(let (
           (,len    (length ,array))
           (,step   (or ,step-exp 1))
           ,limit
           ,idx-var
           )
       (cond
        ((< 0 ,step)
         (setq ,idx-var (or (seq/idx-as-pos-idx ,len ,start-exp)    0)
               ,limit   (or (seq/idx-as-pos-idx ,len ,limit-exp) ,len)
               )
         (cl-assert (<= 0 ,idx-var ,limit) t "array/do-tween-idx*; forward parameters out of range.")
         (or
          (catch '--cl-block-nil--;;  Maybe should use cl-block, or different symbol name?
            (while (< ,idx-var ,limit)
              (cl-symbol-macrolet ((,elem-var (aref ,array ,idx-var)))
                ,@body
                )
              (setq ,idx-var (+ ,idx-var ,step))
              (if (< ,idx-var ,limit)
                  ,tween
                (throw '--cl-block-nil-- nil)
                )
              ))
          ,result
          ))
        (t
         (or (/= ,step 0) (error "list/do-tween-idx*; Zero step size"))
         (setq ,idx-var (if ,start-exp (seq/idx-as-pos-idx ,len ,start-exp) (1- ,len))
               ,limit   (if ,limit-exp (seq/idx-as-pos-idx ,len ,limit-exp)       -1 )
               )
         (cl-assert (<= -1 ,limit ,idx-var) t "array/do-tween-idx*; backward parameters out of range.")
         (or
          (unwind-protect
              (catch '--cl-block-nil--
                (while (< ,limit ,idx-var)
                  (cl-symbol-macrolet ((,elem-var (aref ,array ,idx-var)))
                    ,@body
                    )
                  (setq ,idx-var (+ ,idx-var ,step))
                  (if (< ,limit ,idx-var)
                      ,tween
                    (throw '--cl-block-nil-- nil)
                    )))
            )
          ,result
          ))))))



(defmacro array/do (array elem-var &rest body)
  (declare
   (indent 2)
   (debug (form symbolp body))
   )
  (cl-assertf (mutable-symbol? elem-var) "array/do; elem-var should be a mutable symbol, but got:  %S" elem-var)
  (cl-assertf body "array/do; empty body")
  `(array/do-tween ,array ,elem-var nil ,@body)
  )



(defun array/fill0 (array)
  "Set all array elements to zero."
  (fillarray array 0)
  )

(cl-defun array/bufinsert (array &key buf (format-spec "%s") (delim "\t") pre post)
  "Like `list/bufinsert' (which see), but for arrays."
  ;; Achtung!  If you modify behavior, synch up `list/bufinsert'
  (cl-check-type delim string)
  (with-current-buffer* buf
    (when pre (insert (: (t? pre) delim pre)))
    (array/do-tween array elem
        (insert delim)
      (insert
       (format format-spec elem);;  To consider: generize to maybe el-format and maybe cl-format.
       )
      )
    (when post (insert (: (t? post) delim post)))
    ))

(provide 'array-slash)


;;; array-slash.el ends here
