;;; list-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231029
;; Updated: 20231029
;; Version: 0.0
;; Keywords: lists

;;; Commentary:

;;  Functions on lists.
;;
;;  Some functions also happen to work more generally on sequences.
;;
;;  But if you need support for non-list sequence use should call the
;;  seq/ functions (sometimes just aliases) instead.

;;  To figure out: Best way (maybe using defgeneric or cl-defmethod)?
;;  to reduce the redundancy between seq-slash.el and list-slash.el


;;  "Non-specialized" function
;;  To extract duplicates from a list use 'seq/dups' and is brethren.

;;; Reverse Dependencies
;;
;;  The Current 20240201 idea is to place any functions in the seq/ namespace
;;  that dispatch their work to specialized sequence types will be placed
;;  in a file named seq-slash-dispatching.el

;;; Change Log:

;;; Antidependencies
;;  Required by seq-slash

;;; Code:


(require 'let1)
(require 'seq-slash-base-include)
(require 'indentor-slash)
(require 'shortcuts)



;;──────────  list/do loops and helper functions

(defun list/do-idx/as-pos-idx (len idx)
  "Return appropriate positive value of IDX;
for a list of length LEN.

nil IDX is quietly passed through."
  (when idx
    (if (< idx 0)
        (+ idx len)
      idx
      )))

(defmacro list/do-idx (L elem-var idx-spec &rest body)
  "Special case of `list/do-tween-idx' with no TWEEN"
  (declare
   (indent 3)
   (debug (form symbolp (symbolp &optional form form) body))
   )
  (or (symbolp elem-var) (error "list/do-idx elem-var should be a symbol"))
  (or body (error "list/do-idx empty body"))
  `(list/do-tween-idx* ,L ,elem-var ,idx-spec nil nil ,@body)
  )

(defmacro list/do-idx* (L elem-var idx-spec result &rest body)
  "Special case of `list/do-tween-idx*' with no TWEEN"
  (declare
   (indent indentor/1121)
   (debug (form symbolp (symbolp &optional form form) form body))
   )
  (or (symbolp elem-var) (error "list/do-idx* elem-var should be a symbol"))
  (or body (error "list/do-idx empty body"))
  `(list/do-tween-idx* ,L ,elem-var ,idx-spec ,result nil ,@body)
  )


(defmacro list/do-tween (L elem-var tween &rest body)
  "Evalues body for each element of list L,
evaluate TWEEN between iterations.

Normally returns nil, unless `cl-return' call in BODY
is triggered and returns some value.

See also `list/do-tween*' and `list/do-tween-idx'"
  (declare
   (indent indentor/1121)
   (debug (form symbolp form body))
   )
  (or (symbolp elem-var) (error "list/do-tween elem-var should be a symbol"))
  (or body (error "list/do-tween empty body"))
  `(list/do-tween* ,L ,elem-var nil ,tween ,@body)
  )


(defmacro list/do-tween* (L elem-var result tween &rest body)
  "Evalues body for each element of list L,
evaluate TWEEN between iterations

Simplified, special case `list/do-tween-idx*'"
  (declare
   (indent indentor/11231)
   (debug (form symbolp form form body))
   )
  (or (symbolp elem-var) (error "list/do-tween* elem-var should be a symbol"))
  (or body (error "list/do-tween* empty body"))
  (let1  list-ref  (gensym "list/do-tween*/list-ref")
    `(let1 ,list-ref ,L
       (or
        (catch '--cl-block-nil--;;  Maybe should use cl-block, or different symbol name?
          (while ,list-ref
            (cl-symbol-macrolet ((,elem-var (car ,list-ref)))
              ,@body
              )
            (setq ,list-ref (cdr ,list-ref))
            (if ,list-ref
                ,tween
              (throw '--cl-block-nil-- nil)
              )))
        ,result
        ))))


(defmacro list/do-tween-idx (L elem-var idx-spec tween &rest body)
  "Like `list/do-tween-idx*', but normally returns nil"
  (declare
   (indent indentor/11121)
   (debug (form symbolp (symbolp &optional form form) form body))
   )
  (or  (symbolp elem-var)
       (error "list/do-tween-idx elem-var should be a symbol but received: %S" elem-var))
  (and (symbolp tween)
       (error "list/do-tween-idx, does not make sense for TWEEN to be a symbol, but received: %S" tween))
  (or body (error "list/do-tween-idx empty body"))
  `(list/do-tween-idx* ,L ,elem-var ,idx-spec nil ,tween ,@body)
  )


(defmacro list/do-tween-idx* (L elem-var idx-spec result tween &rest body)
  "Evaluate BODY for indices of list L, with element value
bound to ELEM-VAR its index bound to IDX-VAR.

TWEEN should be a form to be executed between iterations
of the loop. The binding ELEM-VAR is not visible to code
in TWEEN.

Negative values of START and LIMIT are treated as counting
from the end of the list;
i.e. -1 for the final element of L, -2 for the penultimate one...

In the loop, ELEM-VAR is generalized variable;
so the contents of list L can be changed by setting ELEM-VAR

\(fn L ELEM-VAR (IDX-VAR [STEP [START [LIMIT]]]) RESULT TWEEN BODY)"
  (declare
   (indent indentor/111321)
   (debug (form symbolp (symbolp &optional form form) form form body))
   )
  (cl-assertf (mutable-symbol? elem-var) "list/do-tween*; elem-var should be a mutable symbol, but got:  %S" elem-var)
  (cl-assertf body                       "list/do-tween*; empty body")
  (let (
        (idx-var      (nth 0 idx-spec))
        (step-exp     (nth 1 idx-spec))
        (start-exp    (nth 2 idx-spec))
        (limit-exp    (nth 3 idx-spec))
        (step      (gensym "list/do-tween-idx*/step"))
        (limit     (gensym "list/do-tween-idx*/limit"))
        (len       (gensym "list/do-tween-idx*/len"))
        (list-head (gensym "list/do-tween-idx*/list-head"))
        (list-ref  (gensym "list/do-tween-idx*/list-ref"))
        )
    (cl-assert (symbolp elem-var) nil (format "Expected (nth 0 idx-spec) to be a symbol but got %S" elem-var))
    (cl-assert (symbolp idx-var)  nil (format "Expected (nth 1 idx-spec) to be a symbol but got %S" idx-var ))
    `(let (
           (,len    (length ,L))
           (,step   (or ,step-exp 1))
           ,list-head
           ,list-ref
           ,limit
           ,idx-var
           )
       (cond
        ((< 0 ,step)
         (setq ,idx-var (or (seq/idx-as-pos-idx ,len ,start-exp)    0)
               ,limit   (or (seq/idx-as-pos-idx ,len ,limit-exp) ,len)
               )
         (setq ,list-ref  (nthcdr ,idx-var ,L))
         (cl-assert (<= 0 ,idx-var ,limit) t "list/do-tween-idx*; forward parameters out of range.")
         (or
          (catch '--cl-block-nil--;;  Maybe should use cl-block, or different symbol name?
            (while (< ,idx-var ,limit)
              ;; cl-symbol-macrolet used here instead of let, so that list elements
              ;; can be modified in body as in  (setq elem-var new-value)
              ;; Placing it here in the inner loop might slow things down;
              ;; in interpreted code for sure, not sure about for byte compiled code.
              ;; Todo: profile and possibly figure out how to move the cl-symbol-macrolet
              ;; expansion up above the while loop.
              (cl-symbol-macrolet ((,elem-var (car ,list-ref)))
                ,@body
                )
              (setq ,idx-var  (+ ,idx-var ,step)
                    ,list-ref (nthcdr ,step ,list-ref)
                    )
              (if (< ,idx-var ,limit)
                  ,tween
                ;; Without the following throw, emacs 28.2 ert test byte compilation gave
                ;; Warning: value returned from (< i list/do-tween-idx*/limit294427) is unused
                (throw '--cl-block-nil-- nil)
                )
              ))
          ,result
          ))
        (t
         (or (/= ,step 0) (error "list/do-tween-idx*; Zero step size"))
         (setq ,step (- ,step));; make step positive for nthcdr acting on reversed list inside loop below.
         (setq ,idx-var (if ,start-exp (seq/idx-as-pos-idx ,len ,start-exp) (1- ,len))
               ,limit   (if ,limit-exp (seq/idx-as-pos-idx ,len ,limit-exp)       -1 )
               )
         (cl-assert (<= -1 ,limit ,idx-var) t "list/do-tween-idx*; backward parameters out of range.")
         (setq ,list-head (nreverse ,L))
         (setq ,list-ref (nthcdr (- ,len ,idx-var 1) ,list-head))
         (or
          (unwind-protect
              (catch '--cl-block-nil--
                (while (< ,limit ,idx-var)
                  (cl-symbol-macrolet ((,elem-var (car ,list-ref)))
                    ,@body
                    )
                  (setq ,idx-var  (- ,idx-var ,step)
                        ,list-ref (nthcdr ,step ,list-ref)
                        )
                  (if (< ,limit ,idx-var)
                      ,tween
                    ;; Without the following throw, emacs 28.2 ert test byte compilation gave
                    ;; Warning: value returned from (< list/do-tween-idx*/limit293047 i) is unused
                    (throw '--cl-block-nil-- nil)
                    )))
            (nreverse ,list-head)
            )
          ,result
          ))))))



(cl-defun list/bufinsert (L &key buf (format-spec "%s") (delim "\t") pre post)
  "Insert list L according to FORMAT-SPEC;
DELIM is inserted in between elements.
when given, PRE and POST are inserted before or after the list.
If their value is t, then DELIM is inserted instead.
Insertion is done in buffer BUF defaulting to the current buffer.
"
  ;; Achtung!  If you modify behavior, synch up `array/bufinsert'
  (cl-check-type delim string)
  (with-current-buffer* buf
    (when pre (insert (: (t? pre) delim pre)))
    (list/do-tween L elem
        (insert delim)
      (insert
       (format format-spec elem);;  To consider: generize to maybe el-format and maybe cl-format.
       )
      )
    (when post (insert (: (t? post) delim post)))
    ))


(defun list/final-item (L)
  "Return final item of list L.
Named last1 in P. Graphs On Lisp Book."
  (car (last L))
  )

(defun list/append1 (L obj)
  "Return new list containing OBJ appended only list L.
From P. Grahams On Lisp book."
  (append L (list obj))
  )

(defun list/as (obj)
  "If OBJ is a list, return it as is,
otherwise return a new list holding just OBJ.

From \"mklist\" in P. Grahams On Lisp book,
but renamed to return OBJ \"as a list\""
  (if (listp obj) obj (list obj))
  )



(cl-defun list/before? (L val1 val2 &key (keyfun #'identity) (test #'equal))
  "True iff VAL1 matches before any match of VAL2 in list L.
When true, the tail of L starting with the first occurrence
of VAL1 is returned.

Inspired by P. Grahams On Lisp book, but differs in detail."
  (cl-check-type L list)
  (while L
    (let1  key  (funcall keyfun (car L))
      (cond
       ((funcall test key val2)
        (cl-return-from list/before? nil)
        )
       ((funcall test key val1)
        (cl-return-from list/before? L)
        ))
      (setq L (cdr L))
      )))


(cl-defun list/before-after? (L val1 val2 &key (keyfun #'identity) (test #'equal))
  "True iff VAL1 and VAL2 first occur in that order list L.
When true, the tail of L starting with the occurrence of VAL2 is returned.

From P. Grahams On Lisp function \"after\"."
  (cl-check-type L list)
  (let1 Lrest (list/before? L val1 val2 :keyfun keyfun :test test)
    (and Lrest (cl-member val2 Lrest :key keyfun :test test))
    ))


(cl-defun list/nth-occurrence (L val n &key (keyfun #'identity) (test #'equal))
  "Return the tail L starting with the Nth occurrence of VAL1,
or nil of no such tail exists."
  (cl-check-type L list)
  (cl-check-type n (integer 1 *))
  (while (setq L (cl-member val L :key keyfun :test test))
    (if (= n 1)
        (cl-return-from list/nth-occurrence L)
      (setq L  (cdr L)
            n  (1- n)
            )
      )))


(defun list/conc1 (L obj)
  "`nconc' OBJ to the end of list L (altering L)
From P. Grahams On Lisp book."
  (nconc L (list obj))
  )

(defun list/last1 (L)
  "Return last element of list.  From On Lisp book."
  (car (last L))
  )


(defun list/len< (L1 L2)
  "Iff list L2 is longer than list L1, return a true value
Current implementation returns the tail of L2 with
no counterpart in L1."
  (cl-check-type L1 list)
  (cl-check-type L2 list)
  (while (and L1 L2)
    (setq L1 (cdr L1)
          L2 (cdr L2)
          )
    )
  L2
  )

(defun list/len≦ (L1 L2)
  "Iff list L2 as long as list L1, return true."
  (cl-check-type L1 list)
  (cl-check-type L2 list)
  (while (and L1 L2)
    (setq L1 (cdr L1)
          L2 (cdr L2)
          )
    )
  (not L1)
  )

(defun list/len= (L1 L2)
  "Iff lists L1 and L2 have equal length, return true."
  (cl-check-type L1 list)
  (cl-check-type L2 list)
  (while (and L1 L2)
    (setq L1 (cdr L1)
          L2 (cdr L2)
          )
    )
  (not (or L1 L2))
  )


(defun list/prune (tree test)
  "Return skeleton of TREE, retaining structure but only atoms passing TEST.
From P. Grahams book On Lisp."
  ;; An iterative approach might be faster??
  (cl-labels ((rec
               (tree acc)
               (cond ((null tree)
                      (nreverse acc)
                      )
                     ((consp (car tree))
                      (rec (cdr tree)
                           (cons (rec (car tree) nil) acc))
                      )
                     (t
                      (rec (cdr tree)
                           (if (funcall test (car tree))
                               acc
                             (cons (car tree) acc)
                             ))))))
    (rec tree nil)
    ))


(defun list/singleton? (L)
  "Return L if it is a list with a single element."
  ;; Similar to function single in P. Grahams On Lisp book,
  ;; but when true returns L instead of t
  (and (consp L) (not (cdr L)) L)
  )


(cl-defun list/split-at (L pred &key (keyfun #'identity))
  "Return pair of lists splitting L in two lists (head tail).
tail starts with the first element in L for which PRED is true.

When there is no such element, head is simply L and tail nil;
otherwise head is a new list, but tail is a suffix of L."
  (let (acc)
    (while L
      (if (funcall pred (funcall keyfun (car L)))
          (cl-return-from list/split-at
            (if acc
                (list (reverse acc) L)
              L
            ))
        (push (pop L) acc)
        ))
    (list L nil)
    ))


(cl-defun list/item-count-ht (L &key (keyfun #'identity) (test #'equal))
  "Return hash table holding element value frequences.
SEQ should be a sequence.

When given, KEYFUN computes a key value to be used for the element,
otherwise the element itself is used as a key.

Uniqueness of key values is determined according to TEST
TEST should a hash comparison function, such as {`equal', `eql', `eq'}"
  (cl-check-type L list)
  (seq/item-count-ht L :keyfun keyfun :test test)
  )


(defun list/max-items-score (L score-fun)
  "Return list ((item1 item2 ...) score) of all highest scoring items in list L.
Score according to (score-fun item).

If L is empty, return special list (null null)

P.Graham's On Lisp mostn."
  ;; Uses a one-pass method
  ;; todo: Profile against the alternative, 2-passes, first to find the max score.
  (cl-check-type L list)
  (ifnot L
      (list nil nil)
    (let  (
           (max-items  (list (car L)))
           (max-score  (funcall score-fun (car L)))
           cur-score
          )
    (dolist (item L)
      (setq cur-score (funcall score-fun item))
      (cond
       ((< max-score cur-score)
        (setq max-items (list item)
              max-score cur-score
              ))
       ((= max-score cur-score)
        (push item max-items)
        )))
    (list (nreverse max-items) max-score)
    )))


(defun list/max-item-score (L score-fun)
  "Return list (item score) of highest scoring item in list L.
Score according to (SCORE-FUN item).

Tie goes to item appearing earlier in the list.
Error if L is empty.

P.Graham's On Lisp most."
  (cl-check-type L list)
  (unless L (error "list/argmax received empty list"))
  (let1 max-items-score (list/max-items-score L score-fun)
    (list (caar max-items-score) (cadr max-items-score))
  ))

(defun list/argmax (L score-fun)
  "Like `list/max-item-score', but only returns the item."
  (car (list/max-item-score L score-fun))
  )

(defun list/first-item-val (L pred)
  "Return (ITEM VAL) list for first ITEM in list L satisfying predicate PRED.
VAL is the true value returned by PRED for that ITEM.

Return nil, if no such ITEM exists.
\"find2\" from P. Graham's On Lisp."
  (when L
    (if-let1 val (funcall pred (car L))
        (list (car L) val)
      (list/first-item-val (cdr L) pred)
      )))


(defun list/sans-extra-parens (L)
  "If list L is ((elem1 ...)), return (elem1 ...);
otherwise just return L.
More precisely if L is a singleton list holding
one element which is itself a list;
return that element."
  ;; Not sure this function really belongs in list-slash.el,
  ;; But it is useful for defining flexible function args lists
  ;; For example:
  ;; (defun my+ (&rest addends)
  ;;   (apply #'+ (list/sans-extra-parens addends))
  ;;   )
  ;; Can be called in two ways
  ;;  (my+ 3 5 2)   --> 10
  ;;  (my+ '(3 5 2) --> 10

  (: (and (length= L 1)
          (listp (car L)))
     (car L)
     L
     ))



;;──────────  pointer to cons related  ──────────
(defun list/link/prev (L link)
  "Cons cell in list L immediately previous to LINK-POINTER.

The value of LINK should be a cons cell.
If L contains LINK as its 2nd or later link,
return the cons cell immediately preceding LINK-POINTER.

Otherwise return nil.

It is okay for L to be a circular list.

Note also that nil will be return if LINK appears at the head
of a non-circular list L.
"
  ;; toConsider: provide a function list/pointer/prev* optimized
  ;;             for linear lists.  But potential speedup is less than 2x
  ;; If L is circular and does not hold LINK,
  ;; by default this function falls into an infinite loop.

  ;; That can be avoided however, by setting maybe-circ?
  ;; at a cost of a higher constant factor in running time.
  ;; Another idea is to take advantage of `list-proper-p' implemented in C in emacs version 27

  ;; In any case, running time would still be linear in the length of L.
  (cl-check-type L list)
  (cl-check-type link cons)
  (and
   L
   (cdr L)
   (if (eq (cdr L) link)
       L;;  case where L should be returned treated separated here to facilitate circularity check below.
     (let1 cur L
       (while (and
               (setq cur (cdr cur));  Loop progress
               (not (eq cur L));  Circularity check
               (not (eq (cdr cur) link))
               )
         )
       (and
        (eq (cdr cur) link)
        cur
        ))))
  )


(provide 'list-slash)

;;; list-slash.el ends here
