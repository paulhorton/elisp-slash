;;; letter-slash-tests.el --- letter/ ert tests  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241213
;; Updated: 20241213
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:



(ert-deftest  letter= ()
  (should
   (equal ?a
      (letter= ?a "a")
      )
   )
  (should
   (equal "a"
      (letter= "a" ?a)
      ))
  (should
   (not
    (letter= ?a "A")
    ))
  )


(ert-deftest  letter/equal ()
  (with-case-table ascii-case-table
    (should
     (let ((case-fold-search t))
       (equal ?a
              (letter/equal ?a "A")
              )))
    (should
     (not
      (let ((case-fold-search nil))
        (equal ?a
               (letter/equal ?a "A")
               ))))
    ))



;;; letter-slash-tests.el ends here
