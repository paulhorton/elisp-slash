;;; string-slash.el --- string related functions  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241212
;; Updated: 20241212
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'cl-macs)
(require 'let1)
(require 'until)
(require 'shortcuts);; for  `:', `or='
(require 'char-slash)


(defun string/? (arg)
  "If ARG is a string, return it,
otherwise return nil."
  (and (stringp arg) arg)
  )



(defun string/longest-affix-len-of-width≦ (S max-width suffix? tabchar-width)
  "Return a length of longest affix of S with width no more than MAX-WIDTH.
When SUFFIX? try affix means prefix, otherwise suffix.
See also `truncate-string-to-width'"
  ;; toConsider: optional BEG arg
  ;; toConsider: option to use function other whan `char-width'
  (cl-check-type max-width (integer 0 *))
  (let (
        ;; An idx val of -1 corresponds to a length (and width) of 0
        (prev-idx -1)  next-idx
        (prev-width 0) next-width
        (S-len (length S))
        )
    ;; loop invariant: width S[0..prev-idx] ≦ max-width
    ;;                 where S[0..-1] is the empty string.
    (until-done
     (setq next-idx (1+ prev-idx))
     (if (≦ S-len next-idx)
         (setq done? t)
       (setq next-width  (+ prev-width (char/width (aref S (: suffix?
                                                              (1- (- S-len next-idx))
                                                              next-idx
                                                              ))
                                                   tabchar-width
                                                   )))
       (if (≦ next-width max-width)
           (setq prev-width next-width
                 prev-idx   next-idx)
         ;; else too wide
         (setq done? t)
         )))
    (1+ prev-idx);  1+ to convert idx to length
    ))

(defun string/longest-prefix-len-of-width≦ (S max-width &optional tabchar-width)
  "Return a length of longest prefix of S with width no more than MAX-WIDTH.
See also `truncate-string-to-width'"
  (cl-check-type max-width (integer 0 *))
  (string/longest-affix-len-of-width≦ S max-width nil tabchar-width)
  )

(defun string/longest-suffix-len-of-width≦ (S max-width &optional tabchar-width)
  "Return a length of longest suffix of S with width no more than MAX-WIDTH.
See also `truncate-string-to-width'"
  (cl-check-type max-width (integer 0 *))
  (string/longest-affix-len-of-width≦ S max-width  t  tabchar-width)
  )



(cl-defun string/contains? (S query &key ignore-case? start-pos)
  "If string S contains string-or-char QUERY return the match start position;
otherwise return nil.
while searching, `case-fold-search' is set to IGNORE-CASE?
and START-POS is forwarded to `string-match'.

Does not change the match data.

Similar to `s-contains?' from s.el, except QUERY can be a character.
"
  ;; This function can be useful to test for set membership,
  ;; when treating string S as a set of characters.
  ;;
  ;; To consider is :start-pos the best name for that option?
  ;; Could, for example, provide :beg :end instead PH20241215
  (declare (pure t) (side-effect-free t))
  (cl-check-type S string)
  (cl-check-type query char-or-string)
  (save-match-data
    (let1 case-fold-search ignore-case?
      (and
       (string-match (regexp-quote
                      (or (string/? query)
                          (char-to-string query)
                          ))
                     S
                     start-pos
                     )
       (match-beginning 0)
       ))))


(defun string/bref/summary-stats-sg (S &optional beg end ellipsis newline-indicator)
  "Return string representing stats of a substring of S.
The substring used is [beg end) of S.
The stats include number of newlines and characters."
  (cl-check-type S string)
  (or= beg 0)
  (or= end (length S))
  (or= ellipsis "…")
  (or= newline-indicator "L")
  (or (≦ 0 beg end (length S))
      (error "beg=%d, end=%d or S=%d length inconsistent"
             beg end (length S)
             ))
  (let* (
         (subsg (substring S beg end))
         (num-newlines (cl-count ?\n subsg))
         (num-chars    (length subsg))
         )
    (if (= beg end)
        ""
      (concat
       ellipsis
       (and (< 0 num-newlines) (format "%d%s" num-newlines newline-indicator))
       (format "%d" num-chars)
       ellipsis
	))))



(defun string/bref-aux (flank-sg)
  "Return FLANK-SG contents,
but tabs and newlines replaced with
indicator chars ⭾, and ␤
"
  (ifnot (string-match-p "[\n\t]" flank-sg)
      flank-sg
    ;; toConsider: instead of `subst-char-in-string' use replace funtion in tr.el?
    (let1 retval (copy-sequence flank-sg)
      (subst-char-in-string ?\n ?␤ retval t)
      (subst-char-in-string ?\t ?⭾ retval t)
      retval
      )))


(cl-defun string/bref (S width &key (ellipsis "…") (tabchar-width 2))
  "Return a one-line string of width no more than WIDTH, reflecting
the contents of string S;
Usually a prefix and suffix of S and indication of its length.

Tab characters are replaced with \"⭾\" and newlines with \"␤\".
If S holds newlines and WIDTH is wide enough, the number of newlines
it holds is also indicated.

TABCHAR-WIDTH controls how wide ?⭾ is considered to be.
"
  ;; toConsider:
  ;; * optional argument to control balance between start and end width
  ;; * optional arguments to control treatment of tab and newline characters
  ;; * another optional argument BUFFER, to control the current buffer used with calling char-width
  (cl-check-type S string "string/bref:")
  (cl-check-type width (integer 10 *));  Ensure WIDTH is big enough to look reasonable.
  (cl-check-type ellipsis string)
  (if (≦ (string-width S) width)
      (string/bref-aux S)
    (let (
          (flank{-len 0)
          (flank}-len 0)
          (S-len (length S))
          flank{-width flank}-width
          flanks-width
          (mid-width 0)
          mid-width-prev
          )
      (do-repeat-while (< mid-width mid-width-prev)
        (setq mid-width  (string-width
                          (string/bref/summary-stats-sg S flank{-len (- S-len flank}-len) ellipsis)
                          )
              flanks-width  (- width mid-width)
              flank{-width  (ceiling (/ flanks-width 2.0))
              flank}-width  (floor   (/ flanks-width 2.0))
              flank{-len  (string/longest-prefix-len-of-width≦ S flank{-width tabchar-width)
              flank}-len  (string/longest-suffix-len-of-width≦ S flank}-width tabchar-width)
              mid-width-prev  mid-width
              ))
      (concat (string/bref-aux (substring S 0 (1+ flank{-len)))
              (string/bref/summary-stats-sg S flank{-len (- S-len flank}-len) ellipsis)
              (string/bref-aux (substring S (- S-len flank}-len))
              )))))



(defun string/concat/safe (&rest items)
  "Return ITEMS concatenated as a string.
ITEMS may be nil, an `insert'able thing or,
a cons cell with an `insert'able car.
nil arguments are silently skipped.
"
  (with-temp-buffer
    (dolist (item items)
      (when item
        (insert  (or (car-safe item) item))
        ))
    (buffer-string)
    ))


(defun string/concat-lines/safe-aux (remove-final-newline? &rest items)
  (with-temp-buffer
    (dolist (item items)
      (when item
        (insert  (or (car-safe item) item)  "\n")
        ))
    (when remove-final-newline?
      (delete-char -1);;  Fixme?  Assumes newline is just a single character.
      )
    (buffer-string)
    ))

(defun string/concat-lines/safe (&rest items)
  "Return ITEMS concatenated as a string.
ITEMS may be nil, an `insert'able thing or,
a cons cell with an `insert'able car.

nil arguments are silently skipped, otherwise
newitems are added after each element (including the last one).
"
  (apply #'string/concat-lines/safe-aux nil items)
  )

(defun string/join-lines/safe (&rest items)
  "Like `string/concat-lines/safe', but without the trailing newline."
  (apply #'string/concat-lines/safe-aux t items)
  )




;; Text Property Related
(defun string/no-props/safe (S)
  "Return copy of S without text properties.  nil quietly passed through."
  (when S (substring-no-properties S))
  )

(defun string/with-props (S props &optional beg end)
  "Return copy of S with text properties PROPS added to it.
See also `propertize'.
"
  (or= beg 0)
  (or= end (length S))
  (let1 new-S (substring S)
    (add-text-properties beg end props new-S)
    new-S
    ))

(defun string/with-props/safe (S props &optional beg end)
  "Return copy of S with text properties PROPS added to it.
nil quietly passed through."
  (when S
    (string/with-props S props beg end)
    ))



(provide 'string-slash)

;;; string-slash.el ends here
