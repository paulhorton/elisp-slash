;;; vec-slash.el --- vectors  -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231224
;; Updated: 20240427
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:



(defun vec/elemwise/reduce (fun &rest args)
  "Elemwise reduce of ARGS using function FUN starting with INIT-VAL.
ARGS should consiste of equal length vectors or scalar numbers.
and should contain at least one vector.
"
  ;; similar to Perl List::MoreUtils::pairwise (when called with two vector)
  (apply #'vec/elemwise/reduce/aux "vec/elemwise/reduce" fun args)
  )

(defun vec/elemwise/reduce/aux  (msg-caller fun &rest args)
  "Elemwise reduce of ARGS using function FUN starting with INIT-VAL.
ARGS should consist of equal length sequencs or scalar numbers.
and should contain at least one sequence.

The resulting vector is returned."
  (cl-assertf args "%s; empty ARGS list" msg-caller)
  ;; ARGS is a list, so process it sequentially,
  (let (
        vec-len
        acc-vec
        cur-arg
        )
    ;; Phase 1. No vector yet seen
    (let (acc-scalar)
    (while (and args (not vec-len))
      (cl-etypecase (setq cur-arg (pop args))
        (sequence
         (setq acc-vec (vconcat cur-arg)
               vec-len (length acc-vec)
               )
         (when acc-scalar
           (array/do acc-vec x (setq x (funcall fun acc-scalar x)))
           ))
        (number
         (setq acc-scalar
               (if acc-scalar
                   (funcall fun acc-scalar cur-arg)
                 cur-arg
                 )))))
    ;; Phase 2. Reduce remaining vectors into acc-vec
    (while args
      (cl-etypecase (setq cur-arg (pop args))
        (vector
         (or (length= cur-arg vec-len) (error "%s; Expected equal length vectors, but got %d and %d" msg-caller vec-len (length cur-arg)))
         (dotimes (i vec-len)
           (aset acc-vec i
                 (funcall fun (aref acc-vec i) (aref cur-arg i))
                 )))
        (list
         (or (length= cur-arg vec-len) (error "%s; Expected equal length sequences, but got %d and %d" msg-caller vec-len (length cur-arg)))
         (dotimes (i vec-len)
           (aset acc-vec i
                 (funcall fun (aref acc-vec i) (pop cur-arg))
                 )))
         (number
          (array/do acc-vec x (setq x (funcall fun x cur-arg)))
          )
         ))
    ;;
    (unless acc-vec (error "%s; No vectors found in argument list" msg-caller))
    acc-vec
    )))



;; ────────────────────  Numerical Functions  ────────────────────


;; TODO benchmark profile the various ways to do vec/sum and vec/prod
;; including using cl-reduce, or mapc with a lamba function etc.
(defun vec/sum (vec)
  (let1 sum 0
    (dotimes (i (length vec))
      (setq sum (+ sum (aref vec i)))
      )
    sum
    ))

(defun vec/prod (vec)
  (let1 prod 1.0
    (dotimes (i (length vec))
      (setq prod (* prod (aref vec i)))
      )
    prod
    ))

(defun vec/+ (&rest args)
  "Return vector holding the vector addition of ARGS,
see `vec/elemwise/reduce' for what ARGS can be."
  (apply #'vec/elemwise/reduce/aux "vec/+" #'+ args)
  )

(defun vec/elemwise* (&rest args)
  "Return vector V such that V[i] is the product of the
ith element of ARGS. See `vec/elemwise/reduce' for more
precise explanation of what ARGS can be."
  (apply #'vec/elemwise/reduce/aux "vec/elemwise*" #'* args)
  )


(defun vec/each⅟! (vec)
  "Replace each element of VEC with its reciprocal."
  (dotimes (i (length vec))
    (aset vec i
         (/ (float (aref vec i)))
         )))


(defun vec/scalar*= (vec &rest scalars)
  "Set VEC to scalar product of itself with SCALARS;
and return VEC."
  (let1 scalars-prod (apply #'* scalars)
    (dotimes (i (length vec))
        (aset vec i
              (* scalars-prod (aref vec i))
        ))
    vec
    ))


(defun vec/scalar* (vec &rest scalars)
  "Return copy of VEC multiplied by SCALARS"
  (apply #'vec/scalar*= (copy-sequence vec) scalars)
  )

(defun vec/inner* (vec1 vec2)
  "Return inner product of vectors VEC1 and VEC2"
  ;;  To Consider: accept non-vector sequence arguments.
  ;;  Shorter, but perhaps slower, implementation would be:
  ;;    (cl-reduce '+ (vec/elemwise* vec1 vec2))
  (ifnot (length= vec1 (length vec2))
      (error "%d, %d ≠ lengths" (length vec1) (length vec2))
    (let1 sum 0
      (dotimes (i (length vec1))
        (setq sum (+ sum (* (aref vec1 i) (aref vec2 i))))
        )
      sum
      )))


;; ──────────  Probability Vector Related Functions  ──────────
(defun vec/prob/normalize (vec)
  "Normalize VEC as a probability vector.
Error signaled if any elements of VEC are negative.
error if any elements of VEC are negative.
Returns VEC."
  (when-let1 neg-elem (seq-find #'cl-minusp vec)
    (error "%g < 0 elem; vec/prob/normalize" neg-elem)
    )
  (let1 multiplier (/ (float (vec/sum vec)))
    (vec/scalar*= vec multiplier)
  )
  vec
  )

(defun vec/prob/normalized (vec)
  "Return copy of VEC normalized as a probability vector.
Error signaled if any elements of VEC are negative."
  (vec/prob/normalize (copy-sequence vec))
  )



(provide 'vec-slash)

;;; vec-slash.el ends here
