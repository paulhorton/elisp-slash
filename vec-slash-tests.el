;;; vec-slash-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231224
;; Updated: 20240427
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'vec-slash)


(ert-deftest vec/+ ()
  (should
   (equal
    [19 13 17]
    (vec/+ [9 3 7] 10)
    ))
  (should
   (equal
    [19 13 17]
    (vec/+ 10 [9 3 7])
    ))
  (should
   (equal
    [7 12 24]
    (vec/+ [3 2 17] [4 10 7])
    ))
  )


(ert-deftest vec/elemwise* ()
  (should
   (equal
    [90 30 70]
    (vec/elemwise* [9 3 7] 10)
    ))
  (should
   (equal
    [90 30 70]
    (vec/elemwise* 10 [9 3 7])
    ))
  (should
   (equal
    [12 20 119]
    (vec/elemwise* [3 2 17] [4 10 7])
    ))
  (should
   (equal
    [12 20 119]
    (vec/elemwise* [3 2 17] '(4 10 7))
    ))
  (should
   (equal
    [120 200 1190]
    (vec/elemwise* 10 '(4 10 7) [3 2 17])
    ))
  )


;;; vec-slash-tests.el ends here
