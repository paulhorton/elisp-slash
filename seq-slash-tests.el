;;; seq-slash-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231012
;; Updated: 20231012
;; Version: 0.0
;; Keywords: sequence test

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'ht-slash)
(require 'seq-slash)
(require 'plist-slash)


(ert-deftest seq/list-vals< ()
  (should
   (equal '(0.2 0.1)
          (seq/list-vals< 0.5 [0.7 0.2 0.9 0.1])
          ))
  )


(ert-deftest seq/item-count-ht ()
  (should
   (ht/=
    (plist/to-ht '(?a 3  ?b 1  ?d 4))
    (seq/item-count-ht "ddddabaa")
    ))
  (should
   (ht/=
    (plist/to-ht '(0 1  1 3  2 5  3 1))
    (seq/item-count-ht
     '(0 1 2 3 4 5 6 7 8 9)
     :keyfun #'cl-isqrt
     )))
  )


(ert-deftest seq/dups/keep-all ()
  (should
   (equal '(c d d a c b b a)
          (seq/dups/keep-all
           '(c j d q u d r a w c o b p b a)
           )
    )
   ))


(ert-deftest seq/dups ()
  (should
   (equal '(c d d a c b b a)
          (seq/dups
           '(c j d q u d r a w c o b p b a)
           )
    ))
  (should
   (equal '(c d d a c b b a)
          (seq/dups
           '(c j d q u d r a d d w c o b p b a)
           :keep 2
           ))
    )
  (should
   (equal '(d c b a)
          (seq/dups
          '(c j d q u d r a d d w c o b p b a)
          :keep -1
          )
    )))


(ert-deftest seq/len< ()
  (should
   (seq/len< nil [a])
   )
  (should
   (seq/len< [a] [b c])
   )
  (should
   (seq/len< [a] '(b c))
   )
  (should
   (seq/len< '(a) "bc")
   )
  (should
   (not
   (seq/len< '(a) "z")
   ))
  (should
   (not
   (seq/len< [a] [z])
   ))
  (should
   (not
    (seq/len< [b c] [a])
   ))
   )


(ert-deftest seq/len≦ ()
  (should
   (seq/len≦ nil [a])
   )
  (should
   (seq/len≦ [a] [b c])
   )
  (should
   (seq/len≦ [a] '(b c))
   )
  (should
   (seq/len≦ '(a) "bc")
   )
  (should
   (seq/len≦ '(a) "z")
   )
  (should
   (seq/len≦ [a] [z])
   )
  (should
   (not
    (seq/len≦ [b c] [a])
    ))
  (should
   (not
    (seq/len≦ '(b c) [a])
    ))
   )


(ert-deftest seq/*scalar/list  ()
  (should
   (equal '(40 30 -50)
          (seq/*scalar/list '(4 3 -5) 10)
          ))
  (should
   (equal '[45 25 35]
          (seq/*scalar [9 5 7] 5)
          ))
  )


(ert-deftest seq/do-tween ()
  (should
   (t?
    (with-temp-buffer/string=
        "cat:dog:horse"
      (seq/do-tween
        '("cat" "dog" "horse")
        s
        (insert ":")
        (insert s)
        )
      (buffer-string)
      )
    ))
  (should
   (t?
    (with-temp-buffer/string=
        "cat:dog:horse"
      (seq/do-tween
        ["cat" "dog" "horse"]
        s
        (insert ":")
        (insert s)
        )
      (buffer-string)
      )
    ))
  )


;;; seq-slash-tests.el ends here
