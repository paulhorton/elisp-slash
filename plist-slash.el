;;; plist-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231020
;; Updated: 20231020
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'let1)
(require 'ifnot)
(require 'ht-slash)


(define-error
  'plist/odd-number-of-items
  "Putative plist has an odd number of items"
  'plist-error
  )

(define-error
  'plist/duplicate-keys
  "Putative plist has duplicate key:"
  'plist-error
  )



(defun plist/to-alist/reversed (plist)
  "Return PLIST as an alist in reversed order
see `plist/to-alist'"
  (let (alist)
      (while plist
        (let1  key  (pop plist)
          (or plist (signal 'plist/odd-number-of-items nil))
          (let1  val  (pop plist)
            (push (cons key val) alist)
            )))
      alist
      ))


(defun plist/to-alist (plist)
  "Return PLIST as an alist"
  (reverse (plist/to-alist/reversed plist))
  )

(defalias 'alist⟵plist 'plist/to-ht)


(cl-defun plist/to-ht (plist &key (keycmp #'equal))
  "Return the contents of property list PLIST as a hash table"
  (let1  ht  (make-hash-table :test keycmp)
    (while plist
      (let1  key  (pop plist)
        (or plist (signal 'plist/odd-number-of-items nil))
        (let1  val  (pop plist)
          (if (gethash key ht)
              (signal 'plist/duplicate-keys key)
            (puthash key val ht)
            ))))
    ht
    ))

(defalias 'ht⟵plist 'plist/to-ht)


(cl-defun plist/sorted/by-key (plist &key (cmp #'string<))
  "Return copy of PLIST sorted by KEY
according to predicate CMP applied to keys"
  (alist/to-plist
   (sort
    (plist/to-alist/reversed plist)
    (lambda (kv1 kv2)
      (funcall cmp (car kv1) (car kv2))
      ))))



(cl-defun plist/⊉ (plist1 plist2 &key (keycmp #'equal) (valcmp #'equal))
    "Is PLIST1 *not* a super set of PLIST2?

In other words, does PLIST2 have any (key val) pair
not equally mirrored in PLIST1?

If so return the first one of those pairs as a cons cell.

Otherwise return nil.

Values are compared using VALCMP.

Keys are compared using KEYCMP, which must be a comparison function
supported by hash tables.

Note that keys in PLIST1 not contained in PLIST2 do not affect the outcome."
    (ifnot plist2
        nil
      (let1  ht1  (ht⟵plist plist1 :keycmp keycmp)
        (while plist2
          (let1  key  (pop plist2)
            (or plist2  (signal 'plist/odd-number-of-items nil))
            (let (
                  (val2  (pop plist2))
                  (kv1  (ht/key-kv ht1 key))
                  )
              (ifnot  kv1
                  (cl-return-from plist/⊉ (cons key val2))
                (let1  val1  (cdr kv1)
                  (unless
                      (funcall valcmp val1 val2)
                    (cl-return-from plist/⊉ (cons key val2))
                    )))))))))



(defun plist/map-reversed (plist fun)
  "Like `plist/map' but results are in reverse order."
  (let (ret-list)
    (while plist
      (push
       (funcall fun
                (prog1 (pop plist)  (or plist (signal 'plist/odd-number-of-items nil)))
                (pop plist)
                )
       ret-list
       ))
    ret-list
    ))

(defun plist/map (plist fun)
  "Return list of results of calling (FUN key val) on each element of PLIST.
See also `map-apply'."
  (reverse
   (plist/map-reversed plist fun)
   ))


(cl-defun plist/insert-into-buf
    (plist buf &key start (keyf "%S") (valf "%S") (kv-sep "\t") (pair-sep "\n") sep-spans)
  "Insert contents of property list PLIST into buffer BUF.
Usually PAIR-SEP is inserted after each key val pair,
but if SEP-SPANS is true, SEP-SPANS is not inserted after the final pair.

Returns where point was in BUF after the insertion."
  ;; To consider, supporting cl-format
  (cl-check-type plist plist)
  (cl-check-type buf buffer)
  (with-current-buffer buf
    (if start (goto-char start))
    ;; to do, use with amalgamated undo
    (when plist
      (while plist
        (let1  key (pop plist)
          (or plist (signal 'plist/odd-number-of-items nil))
          (insert (format keyf key)
                  kv-sep
                  (format valf (pop plist))
                  pair-sep
                  )))
      (when sep-spans;  Erase the last PAIR-SEP
        (delete-char (- (length pair-sep)))
        )
      )
    (point)
    ))



(provide 'plist-slash)

;;; plist-slash.el ends here
