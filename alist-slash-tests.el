;;; alist-slash-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231021
;; Updated: 20231021
;; Version: 0.0
;; Keywords: alist test

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'alist-slash)


(ert-deftest alist/sprintf ()
  (should
   (equal
      '("fred::27"
        "ted::92")
      (let1 alist '((fred 27) crap (ted . 92))
        (alist/sprintf alist "%s" "%d" "::")
        )
      )))


(ert-deftest alist/assocs ()
    (should
     (equal
      '((b B) (b β beta))
      (alist/assocs
       '((b B) (a A) (c C) (b β beta))
       'b
       )
       ))
    )


(ert-deftest alist/keys ()
    (should
     (equal '(k1 k2 k3)
            (alist/keys
             '((k1 . val1) junk (k2 val2) more-junk (k3 . val3))
             )
      )))


(ert-deftest alist/dups ()
  (should
   (equal '((3 E) (3 U) (5 G) (5 H))
          (alist/dups
           '((1 A) (2 B) (3 E) (4 Z) (3 U) (6 I) (5 G) (5 H))
           ))
    ))


(ert-deftest alist/to-plist ()
  (should
   (equal
    '(pine cones  oak acorns  maple seeds)
    (alist/to-plist
     '((pine . cones)
       (oak . acorns)
       (maple . seeds))
     )
    ))
  )


(ert-deftest alist/to-plist/with-redundant-keys ()
  (should
   (equal
    '(pine cones  oak acorns  maple seeds)
    (alist/to-plist
     '((pine . cones); Modern form
       (oak . acorns)
       (maple . seeds)
       ;; Outdated versions at the end of the alist
       (pine . pinhnyte); Old English
       (oak . æcern)
       (maple . sēd)
       )
     )
    ))
  )


(ert-deftest alist/to-plist/with-crap ()
  ;; Try an alist with non-cons-cell crap in it (which should be quietly ignored)
  (should
   (equal
    '(pine cones  oak acorns  maple seeds)
    (alist/to-plist
     '((pine . cones)
       "crappy string"
       (oak . acorns)
       (maple . seeds)
       crappy-symbol
       ;; do not try 'crappy-symbol here expecting it to be ignored,
       ;; because that would expand to (quote crappy-symbol) which is in fact a cons cell!
       ;; Been there, done that.
       )
     )
    ))
  )



;;; alist-slash-tests.el ends here
