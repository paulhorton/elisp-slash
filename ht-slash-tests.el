;;; ht-slash-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231018
;; Updated: 20231018
;; Version: 0.0
;; Keywords: hash table tests.

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'ht-slash)
(require 'plist-slash)

(ert-deftest ht/key-kv ()
  (should
   (equal
    '(3 . dog)
    (ht/key-kv
     (ht⟵plist '(1 cat  2 pig  3 dog  4 horse))
     3)))
  (should
   (equal
    '(3 . nil)
    (ht/key-kv
     (ht⟵plist '(1 cat  2 pig  3 nil  4 horse))
     3)))
  (should
   (null
    (ht/key-kv
     (ht⟵plist '(1 cat  2 pig  3 dog  4 horse))
     5)
    )))


(ert-deftest ht/to-alist ()
  (should
   (equal
    '((?1 . 1) (?2 . 2) (?3 . 3) (?4 . 4))
    (sort-by-car
     (let1  ht  (make-hash-table)
       (puthash ?1 1 ht)
       (puthash ?2 2 ht)
       (puthash ?3 3 ht)
       (puthash ?4 4 ht)
       (ht/to-alist ht)
       )
     #'<
     ))
   ))


(ert-deftest ht/to-alist ()
  (should
   (equal
    '((?1 . 1) (?2 . 2) (?3 . 3) (?4 . 4))
    (sort-by-car
     (let1  ht  (make-hash-table)
       (puthash ?1 1 ht)
       (puthash ?2 2 ht)
       (puthash ?3 3 ht)
       (puthash ?4 4 ht)
       (ht/to-alist ht)
       )
     #'<
     ))
   ))


(ert-deftest ht/++ ()
  (equal
   '("aa" 1  "at" 2  "ta" 2)
   (let1  dimer-counts  (make-hash-table :test #'equal)
     ;; tataat
     (ht/++ dimer-counts "ta")
     (ht/++ dimer-counts "at")
     (ht/++ dimer-counts "ta")
     (ht/++ dimer-counts "aa")
     (ht/++ dimer-counts "at")
     (plist/sorted/by-key (plist⟵ht dimer-counts))
     )))


(ert-deftest ht/⊉ ()
  (should
   (memq
    (ht/⊉
     (ht⟵plist '(a 5 b 9))
     (ht⟵plist '(a 5 b 9 c 12 d 6))
     )
    '(c d)
    ))
(should
   (null
    (ht/⊉
     (ht⟵plist '(a 5 b 9))
     (ht⟵plist '(a 5 b 9))
     )
    )))



;;; ht-slash-tests.el ends here
