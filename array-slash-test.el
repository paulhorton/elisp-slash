;;; array-slash-test.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240427
;; Updated: 20240427
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'array-slash)


(ert-deftest array/do-idx/forward-step1 ()
  (should
   (equal
    '((20 0) (30 1) (70 2))
    (let (acc)
      (array/do-idx*
        [20 30 70]
        x
          (i)
        (reverse acc)
        (push (list x i) acc)
       )
      )
    )))


(ert-deftest array/do-idx*/bakward-step1 ()
  (should
   (equal
    '((70 2) (30 1) (20 0))
    (let (acc)
      (array/do-idx*
        [20 30 70]
        x
          (i)
        acc
        (push (list x i) acc)
        )
      )
    )))


(ert-deftest array/do-idx*/forward-step2 ()
  (should
   (equal
    '(:a :c :d extra)
    (let (acc)
      (array/do-idx*
        [:a apple :c cat :d dog extra]
        x
          (i 2)
        (reverse acc)
        (push x acc)
        ))
    ))
  (should
   (equal
    '(apple cat dog)
    (let (acc)
      (array/do-idx*
        [:a apple :c cat :d dog extra]
        x
          (i 2 1)
        (reverse acc)
        (push x acc)
        ))
    ))
  )


(ert-deftest array/do-idx*/bakward-step2 ()
  (should
   (equal
    '(:a :c :d extra)
    (let (acc)
      (array/do-idx*
        [:a apple :c cat :d dog extra]
        x
          (i -2)
        acc
        (push x acc)
        ))
    ))
  (should
   (equal
    '(apple cat dog)
    (let (
          (array  [:a apple :c cat :d dog extra])
          acc
          )
      (array/do-idx*
        array
        x
          (i -2  (- (length array) 2))
        acc
        (push x acc)
        ))
    ))
  )




;;; array-slash-test.el ends here
