;;; letter-slash.el --- meta type representing a letter  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241211
;; Updated: 20241211
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;  Meta type including Elisp characters and Elisp length one strings.

;;  Intended to cushion the type barrier
;;  when going back and forth between representations; for example, ?a and "a".

;;; Change Log:

;;; Code:




(defun letterp (arg)
  "If ARG is a letter, return it;
otherwise return nil"
  (and (or (characterp arg)
           (and (stringp arg) (length= arg 1))
           )
       arg
       ))



;; ──────────  Pseudo-accessors  ──────────
(defun letter/char (arg)
  "Return letter ARG as a character."
  (if (characterp arg)
    arg
    (if (and (stringp arg) (length= arg 1))
        (aref arg 0);;  Could use `char-to-string' here, but aref has a bytecode OP so might be faster(?)
      (signal 'wrong-type-argument (list 'letterp arg))
      )))


(defun letter/char-safe (arg)
  "If ARG is a letter, return it as a character.
Otherwise return nil."
  (if (characterp arg)
      arg
    (and (stringp arg)
         (length= arg 1)
         (aref arg 0)
         )))


(defun letter/string (arg)
  "Return letter ARG as a string"
  (if (characterp arg)
      (char-to-string arg)
    (if (and (stringp arg) (length= arg 1))
        arg
      (signal 'wrong-type-argument (list 'letterp arg))
      )))


(defun letter/string-safe (arg)
  "If ARG is a letter, return it as a string.
Otherwise return nil.
"
  (if (characterp arg)
      (char-to-string arg)
    (and (stringp arg) (length= arg 1) arg)
      ))



(defun letter= (arg1 arg2)
  "If ARG1 and ARG2 are hold the same letter, return ARG1;
otherwise return nil.
"
  ;; To Consider: expand argument list either to accept more args
  (and (=
        (letter/char arg1)
        (letter/char arg2)
        )
       arg1
       ))


(defun letter/equal (arg1 arg2)
  "Similar to `char-equal', but
1. Accepts strings holding one character.
2. When true, returns the first argument.
"
  ;; To Consider: expand argument list either to accept more args
  (and (char-equal
        (letter/char arg1)
        (letter/char arg2)
        )
       arg1
       ))



(provide 'letter-slash)

;;; letter-slash.el ends here
