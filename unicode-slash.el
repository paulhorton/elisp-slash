;;; unicode-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231208
;; Updated: 20231208
;; Version: 0.0
;; Keywords: unicode characters

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'shortcuts)


(defvar unicode/fraction-char/regex
  "[½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅛⅜⅝⅞⅑⅒]"
  "Regex matching a unicode fraction character"
  )

(defvar unicode/mixed-fraction/regex
  (concat "[+-]?[0-9]*" unicode/fraction-char/regex)
  "Regex matching a unicode fraction character"
  )


(defvar unicode/decimal-of-fraction/alist
  '(
    ;; Leading zeros intentionally omitted from string values.
    ;; (concat "1" ".5") --->  1.5
    ("½" . ".5")
    ("⅓" . ".3333333333333333")  ("⅔" . ".6666666666666667")
    ("¼" . ".25")  ("¾" . ".75")
    ("⅕" . ".2")  ("⅖" . ".4")  ("⅗" . ".6")  ("⅘" . ".8")
    ("⅙" . ".16666666666666667")  ("⅚" . ".8333333333333333")
    ("⅐" . ".14285714285714286")
    ("⅛" . ".125")  ("⅜" . ".375")  ("⅝" . ".625")  ("⅞" . ".875")
    ("⅑" . ".1111111111111111")
    ("⅒" . ".1")
    (½ . 0.5)
    (⅓ . 0.3333333333333333)  (⅔ . 0.6666666666666667)
    (¼ . 0.25)  (¾ . 0.75)
    (⅕ . 0.2)  (⅖ . 0.4)  (⅗ . 0.6)  (⅘ . 0.8)
    (⅙ . 0.16666666666666666)  (⅚ . 0.8333333333333333)
    (⅐ . 0.14285714285714285)
    (⅛ . 0.125)  (⅜ . 0.375)  (⅝ . 0.625)  (⅞ . 0.875)
    (⅑ . 0.1111111111111111)
    (⅒ . 0.1)
    )
  "alist mapping unicode fraction characters to a decimal representation."
  ;; To consider, adding character keys such as ?½
  ;; not sure if the best value would be the string ".5", or the float .5
  )

(defun unicode/decimal-string-of-mixed-fraction (unicode-mixed-frac-sg)
  "Return string representing string UNICODE-MIXED-FRAC-SG in decimal form."
  (cl-check-type unicode-mixed-frac-sg string)
  (if (empty? unicode-mixed-frac-sg)
      unicode-mixed-frac-sg
    (save-match-data
      (if (string-match unicode/mixed-fraction/regex unicode-mixed-frac-sg)
          (concat (substring unicode-mixed-frac-sg 0 -1)
                  (cdrassoc-string (substring unicode-mixed-frac-sg -1)
                                unicode/decimal-of-fraction/alist
                                ))
        unicode-mixed-frac-sg
        ))))

(defun unicode/decimal-string-of-mixed-fractions (unicode-mixed-fracs-sg)
  "Return string representing string UNICODE-MIXED-FRACS-SG in decimal form.
all mixed fractions in UNICODE-MIXED-FRACS-SG are converted."
  (replace-regexp-in-string
   unicode/mixed-fraction/regex
   #'unicode/decimal-string-of-mixed-fraction
   unicode-mixed-fracs-sg
   )
  )



(provide 'unicode-slash)

;;; unicode-slash.el ends here
