;;; string-slash-test.el --- ert test for string-slash  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241212
;; Updated: 20241212
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'string-slash)



(ert-deftest string/contains? ()
  (should
   (= 2
    (string/contains? "schweinehund" ?h)
    ))
  (should
   (not
    (string/contains? "schweinehund" ?x)
    ))
  (should
   (= 2
    (string/contains? "schweinehund" "h")
    ))
  (should
   (= 3
    (string/contains? "schweinehund" "weine")
    ))
   (should
    (not
     (string/contains? "schweinehund" "wine")
     ))
   )



;;; string-slash-test.el ends here
