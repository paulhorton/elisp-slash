;;; buf-or-string-slash.el --- functions acting on a buffer or sequence  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241006
;; Updated: 20241006
;; Version: 0.0
;; Keywords:

;;; Commentary:

;; Some elisp builtin commands handle objects which can be buffers or strings.
;; for example, text property handling functions such as `get-text-property'

;; That suggests it might be useful to define a meta type including both buffers and strings.

;; The meta type buf-or-string should be either a string,
;  a buffer, or nil to indicate the current buffer.
;; The use of nil here follows the convention of some elisp functions,
;; including match-string and text property handling functions,

;; When buf-or-string is intended to be a buffer, the buffer itself (not its name) must be used.

;;; Change Log:

;;; Code:


(require 'shortcuts);  For `with-current-buffer*'.



(defun buf-or-string/idx-in-range? (buf-or-string idx)
  "If IDX is in range, return it; otherwise return nil
\"in range\" means:
  For a buffer:
    point at position IDX `char-after' would return a character.
    (Note that buffer narrowing can affect the result)
  For a string:
    `aref' IDX would return a character."
  (if (stringp buf-or-string)
      (< -1 idx (length buf-or-string))
    (with-current-buffer* buf-or-string
      (<  (1- (point-min))  idx  (point-max))
      )))



(provide 'buf-or-string-slash)

;;; buf-or-string-slash.el ends here
