;;; regex-slash-test.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240526
;; Updated: 20240526
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'regex-slash)


(ert-deftest regex/match? ()
  (should
   (string=
    "cat"
    (regex/match?  "cat"  "the cat in the hat")
   ))
  (should
   (eq nil
       (let (case-fold-search)
         (regex/match?  "cat"  "the CAT in the hat")
         )
       ))
  )


(ert-deftest regex/cmatch? ()
  (should
   (string=
    "CAT"
    (regex/cmatch?
     ascii-case-table
     "cat"  "the CAT in the hat"
    )
   ))
  )


(ert-deftest regex/imatch? ()
  (should
   (string=
    "CAT"
    (with-case-table ascii-case-table
      (let (case-fold-search)
        ;; imatch should still be case insensitive
        (regex/imatch?  "cat"  "the CAT in the hat")
        ))
    ))
  )


;;; regex-slash-test.el ends here
