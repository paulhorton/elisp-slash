;;; seq-slash-base-include.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231212
;; Updated: 20231212
;; Version: 0.0
;; Keywords: sequence

;;; Commentary:

;; File to hold implementation of seq/ namespace functions
;; which do not dispatch to implementations for specialized types of sequences.
;;
;; Thus files such as list-slash.el or vec-slash.el can safely require
;; this file without concern for circular dependences.
;;
;; Functions in the seq/ namespace which *do* displatch to implementations
;; of specialized types of sequences should be places in seq-slash.el
;;
;;; Change Log:

;;; Antidependencies
;;  Required by list-slash, seq-slash

;;
;;; Code:


(defun seq/idx-as-pos-idx (len idx)
  "Return appropriate positive value of IDX;
for a sequence of length LEN.

nil IDX is intentionally quietly passed through."
  (when idx
    (if (< idx 0)
        (+ idx len)
      idx
      )))


(cl-defun seq/item-count-ht (seq &key (keyfun #'identity) (test #'equal))
  "Return hash table holding element value frequences.
SEQ should be a sequence.

When given, KEYFUN computes a key for an element;
otherwise the element itself.

Uniqueness of key values is determined according to TEST
TEST should a hash comparison function, such as {`equal', `eql', `eq'}"
  (cl-check-type seq sequence)
  (let (
        (key-counts-ht  (make-hash-table :test test :rehash-size 2.0))
        (cur-key)
        )
    (mapc
     (lambda (elem)
       (setq cur-key (funcall keyfun elem))
       (puthash cur-key
                (1+ (gethash cur-key key-counts-ht 0))
                key-counts-ht
                ))
     seq
     )
    key-counts-ht
    ))


(cl-defun seq/dups/keep-all (seq &key (keyfun #'identity) (test #'equal))
  "Return list of duplicate items in sequence SEQ.
Comparison is done by TEST on values returned by function KEYFUN
(funcall KEYFUN elem) should be an idempotent and return a value
comparable with TEST.

TEST should one of {`equal', `eql', `eq'} or other hash comparison function.
See also `list/dumps'
"
  (let1  val-counts-ht  (seq/item-count-ht seq :keyfun keyfun :test test)
    (seq-filter
     (lambda (elem)
       (< 1 (gethash (funcall keyfun elem) val-counts-ht))
       )
     seq
     )))


(cl-defun seq/dups (seq &key (keyfun #'identity) (test #'equal) keep)
  "Return list of duplicate items in sequence SEQ.
Comparison is done by TEST on values returned by function KEYFUN
(funcall KEYFUN elem) should be an idempotent and return a value
comparable with TEST.

TEST should one of {`equal', `eql', `eq'} or other hash comparison function.
l
When given, KEEP should be an integer representing the number of
occurrences of each duplicate keyfun to return;
the first (or last if KEEP negative) duplicates are returned.
"
  (cl-check-type seq sequence)
  (ifnot keep
      (seq/dups/keep-all seq  :keyfun keyfun  :test test)
    (if (zerop keep)
        nil; keep zero duplicates, i.e. keep nothing.
      (if (< keep 0)
          (seq/dups/reversed (reverse seq) (- keep)  :keyfun keyfun  :test test)
        (reverse
         (seq/dups/reversed  seq  keep  :keyfun keyfun  :test test)
         )
        ))))


(cl-defun seq/dups/reversed (seq keep &key (keyfun #'identity) (test #'equal))
  "Like `seq/dups' but return list reversed, and KEEP > 0 is mandatory."
  (cl-check-type keep (integer 1 *))
  (let (
        (val-counts-ht  (seq/item-count-ht seq  :keyfun keyfun  :test test))
        (retlist '())
        )
    (seq-doseq (elem seq)
      (let1  computed-key  (funcall keyfun elem)
        (let1  count-or-x  (gethash computed-key val-counts-ht)
          ;; upon first encounter of key, count-or-x is its count
          ;; a count of one means do nothing (it is not a dup)
          ;; Otherwise that place in the hash table is recycled to hold
          ;; a book keeping value x, incremented until it reaches 1.
          (when (/= 1 count-or-x)
            (puthash computed-key
                     (if (< 1 count-or-x)
                         (- 2 keep);  So that (1- keep) increments yields 1
                       (1+ count-or-x);  increment x
                       )
                     val-counts-ht
                     )
            (push elem retlist)
            ))))
    retlist
    ))


(provide 'seq-slash-base-include)

;;; seq-slash-base-include.el ends here
