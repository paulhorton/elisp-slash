;;; char-count-table-slash.el --- char tables with count values  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241218
;; Updated: 20241218
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:



(defvar char-count-table-type
  t;; dummy value, not currently used for anything.
  "Symbol used to mark char-table_s as char-count-table_s."
  )
;; To consider: Add extra slots to hold some information,
;; such as a human-friendly name for the table, or to record a
;; non-zero initial count which may be supported in the future.

;; Currently `char-count-table/new' is just a wrapper around `make-char-table'
;; To Consider: added init-count argument, in case
;; one wants to gives initial counts other than zero.
;; Note that several functions would need to be adjusted.
;; as currently the out-of-band value nil is used to indicate a zero count.
(cl-defun char-count-table/new ()
  "Return new char-table suitable for counting characters."
  (make-char-table 'char-count-table-type)
  )


(defun char-count-table/++ (c-count-tab char)
  "Increment value of character CHAR in CHAR-TABLE;
Or add it to the CHAR-TABLE with a value of 1."
  ;; Note; This code seemingly wastefully calls (aref char-table c) twice,
  ;; but benchmarking showed that to be faster than declaring a local
  ;; variable with let to store the result of  (aref char-table c)
  ;;
  ;; In fact, if speed is crucial, something like the following may be faster
  ;; than calling char-table/++ repeatedly.
  ;;
  ;; This approach uses a temporary variable declared *outside* of the loop.
  ;;
  ;; (let (val)
  ;;    (while CONDITION
  ;;       (setq val (aref c-count-tab (following-char)))
  ;;       (aset c-count-tab (following-char)
  ;;           (if val (1+ val) 1)
  ;;       )
  ;;       ...
  ;;    )); end while
  ;;
  (aset c-count-tab char
        (if (aref c-count-tab char)
            (1+ (aref c-count-tab char))
          ;; else char is not present in c-count-tab
          1
          )))


(defun char-count-table/add-char-count (c-count-tab char addend)
  "Add ADDED to count of character CHAR in C-COUNT-TAB."
  (aset c-count-tab char
        (+ addend
           (or (aref c-count-tab char) 0)
           )))


(cl-defun char-count-table/region/char-counts (beg end &optional fold-case-table)
  "Return char-count-table holding char counts of region BEG END;
If FOLD-CASE-TABLE is a true value, coalesce counts of equivalent
characters into their lower caser representative.

When true FOLD-CASE-TABLE should be t or a case-table;
with t indicating to use `current-case-table'."
  (save-excursion
    (goto-char beg)
    (let1 c-count-table (char-count-table/new)
      (let (val)
        (while (< (point) end)
          (setq val (aref c-count-table (following-char)))
          (aset c-count-table (following-char)  (if val (1+ val) 1))
          (forward-char)
          ))
      (when fold-case-table
        (char-count-table/casefold-counts! c-count-table (true-not-t? fold-case-table))
        )
      c-count-table
      )))


(defun char-count-table/casefold-counts! (c-count-tab &optional case-table)
  "Coalesce counts of equivalent characters into their lower case representative.
CASE-TABLE defaults to `current-case-table'"
  ;; TODO: figure out the correct way to canonicalize chars using case tables.
  ;;       Currently I use `downcase'.
  (with-case-table (or case-table (current-case-table))
    (char-table/call-on-each-char
     (lambda (char val)
       (unless (= char (downcase char))
         (char-count-table/add-char-count
          c-count-tab
          (downcase char)
          val
          )
         (aset c-count-tab char nil)
         ))
     c-count-tab
     )))



(provide 'char-count-table-slash)

;;; char-count-table-slash.el ends here
