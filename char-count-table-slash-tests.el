;;; char-count-table-tests.el ---  ert tests for char-count-table -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20241218
;; Updated: 20241218
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'let1)
(require 'char-count-table-slash)


(ert-deftest char-count-table/++ ()
  (should
   (equal
    [3 1 nil]
    (let1 counts (make-char-table 'ert-char-count-table/++)
      (char-count-table/++ counts ?a)
      (char-count-table/++ counts ?b)
      (char-count-table/++ counts ?a)
      (char-count-table/++ counts ?a)
      (vector (aref counts ?a)
              (aref counts ?b)
              (aref counts ?x)
              )
      )
    )))



;;; char-count-table-tests.el ends here
