;;; plist-slash-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231020
;; Updated: 20231020
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest plist/to-alist ()
  (should
   (equal
    '((a . "apple")
      (b . "boy")
      (d . "pig"); in Taiwanese
      )
    (plist/to-alist '(a "apple" b "boy" d "pig"))
    )
   ))


(ert-deftest plist/sorted/by-key ()
  (should
   (equal '(a 65 b 78 d 20)
          (plist/sorted/by-key '(d 20 b 78 a 65))
          )))


(ert-deftest plist/⊉ ()
  (should
   (equal
    '(c . 12)
    (plist/⊉
     '(a 5  b 9)
     '(a 5  b 9  c 12  d 6)
     )))
  (should
   (null
    (plist/⊉
     '(a 5  b 9)
     '(a 5  b 9)
     )
    )))


;;; plist-slash-tests.el ends here
