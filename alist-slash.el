;;; alist-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Copyright (C) 2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231021
;; Updated: 20231021
;; Version: 0.0
;; Keywords: alist, association list

;;; Commentary:

;; Unlike many lisps,
;; the Elisp allows lists purporting to be alists to include elements which are not cons cells.
;; As documented in the elisp manual,
;; Elisp builtin functions such as assoc quietly ignore such "atomic" elements.
;;
;; Following that Elisp approach, unless explicitly mentioned otherwise, the functions in this file generally
;; quietly skip over atomic elements as well.

;;; Change Log:

;;; Code:


(require 'list-slash)



(cl-defun alist/sprintf (alist key-format val-format &optional (delim "\t"))
  "Return list of strings representing ALIST. One string per element of ALIST;
formated according to KEY-FORMAT and VAL-FORMAT, separated by string DELIM."
  (reverse (alist/sprintf/reversed alist key-format val-format delim))
  )

(cl-defun alist/sprintf/reversed (alist key-format val-format &optional (delim "\t"))
  "Like alist/sprintf, but in reverse order of ALIST"
  (let (acc)
    (dolist (elem alist)
      (when (consp elem);;  Following elisp convention, quietly ignore non cons cell elements.
        (push (format (concat key-format "%s" val-format)
                      (car elem)
                      delim
                      (if (atom (cdr elem))
                          (cdr elem)
                        (cadr elem)
                        ))
              acc
              )))
    acc
    ))



(cl-defun alist/assocs (alist query-item &key (keyfun #'identity) (test #'equal))
  "Similar to `cl-assoc' but returns a list of all matching cons cells.

If ITEM occurs as a key k times in ALIST, those k cons cells are returned.
The KEYFUN and TEST can be stipulated similar to `cl-assoc'.

Like `assoc', non-cons elements of ALIST are quietly ignored.
Note that (unlike assoc) the ALIST argument comes first.
"
  (let1  query-key  (funcall keyfun query-item)
    (delq
     nil
     (mapcar
      (lambda (elem)
        (and
         (consp elem)
         (and (funcall test query-key (funcall keyfun (car elem)))
              elem)
         ))
      alist
      ))))


(defun alist/keys/reversed (alist)
  "Return list of keys of ALIST in reversed order."
  (let (retlist)
    (dolist (elem alist)
      (if (consp elem)
          (push (car elem) retlist)
        )
      )
    retlist
    ))

(defun alist/keys (alist)
  "Return list of keys of ALIST.
Non-cons cells in ALIST are quietly ignored.
Currently returns the cdrs of the cons cells in ALIST,
so, depending on how your alist values are defined,
the car of the returned value may be what you want."
  (reverse (alist/keys/reversed alist))
  )

(defun alist/vals/reversed (alist)
  "Return list of vals of ALIST in reversed order.
See `alist/keys' for more."
  (let (retlist)
    (dolist (elem alist)
      (if (consp elem)
          (push (cdr elem) retlist)
        )
      )
    retlist
    ))

(defun alist/vals (alist)
  "Return list of vals of ALIST."
  (reverse (alist/vals/reversed alist))
  )


(cl-defun alist/dups (alist &key (keyfun #'car) (test #'equal) keep)
  "Return list of items with duplicate keys in alist ALIST.
KEYFUN should be a function of an alist item (e.g. cons cell).
See `seq/dups' for more."
  ;; FIXME?
  ;; keyfun is defined here to receive a (key val) cons cell instead of just a key
  ;; this makes it convenient to forward to `seq/dups'
  ;; but from a users point of view, perhaps it would make more sense for KEYFUN to just recieve a key
  (seq/dups
   (seq-filter #'consp alist)
   :keyfun keyfun  :test test  :keep keep
   ))


(cl-defun alist/to-plist (alist &key (test #'equal))
  "Return a plist holding associations in ALIST;
Redundant keys in ALIST are silently ignored.
TEST is from a hash table used to check for redundancy"
  (let (
        (keys-ht (make-hash-table :test test))
        acc-for-plist
        )
    (dolist (kv alist)
      (when (consp kv)
        (unless (gethash (car kv) keys-ht)
          (puthash (car kv) (cdr kv) keys-ht)
          (push (car kv) acc-for-plist)
          (push (cdr kv) acc-for-plist)
          )))
    (reverse acc-for-plist)
    ))


(cl-defun alist/sort/car-string< (L1 L2)
  "Predicate for `alist/sorted-string<'"
  ;; Near-miss for builtin `car-less-than-car'
  (string< (car L1) (car L2))
  )

(cl-defun alist/sort/cdr> (L1 L2)
  "Predicate for `alist/sorted/by-val>'"
  (> (cdr L1) (cdr L2))
  )

(cl-defun alist/sorted/by-key-string< (alist)
  "Return copy of ALIST sorted by `string<' of its keys.
Current implementation is intolerant of non-cons cell elements in ALIST."
  (sort alist #'alist/sort/car-string<)
  )

(defalias 'alist/sorted/string< 'alist/sorted/by-key-string<)


(cl-defun alist/sorted/by-val> (alist &key)
  "Return copy of ALIST sorted by `>' of its values."
  (sort alist #'alist/sort/cdr>)
  )

(defalias 'alist/sorted/> 'alist/sorted/by-val>)



(provide 'alist-slash)

;;; alist-slash.el ends here
