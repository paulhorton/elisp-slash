;;; char-table-slash-tests.el --- ert tests for char-table  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240517
;; Updated: 20240517
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:

(require 'char-table-slash)




;;; char-table-slash-tests.el ends here
