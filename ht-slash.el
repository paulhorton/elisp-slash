;;; ht-slash.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231016
;; Updated: 20241004
;; Version: 0.0
;; Keywords: hash table

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'let1)


(defalias 'ht/keys 'hash-table-keys)
(defalias 'ht/vals 'hash-table-values)


(defun ht/has-key? (ht key)
  (declare (side-effect-free t))
  ;; Candidate for inline optimization
  "Return a true value iff hashtable HT contains key KEY.

If the key value is nil return t;
Otherwise return the key value.

See also `ht/kv'"
  (or (gethash key ht)
      (not (gethash key ht t))
      )
  ;; Simple, but calls gethash twice when KEY is not HT
  ;; Alternatively could use a dummy not-a-value symbol (interned or not)
  )


(defun ht/key-kv (ht key)
  "If hash table HT contains KEY return cons cell (KEY . VAL);
Otherwise return nil.

See also `ht/has-key?'"
  (if-let1  val-nnil (gethash key ht)
      (cons key val-nnil)
    ;; else key is either nil or absent
    (if (gethash key ht t)
        nil
      (cons key nil)
      )))


(cl-defun ht/to-alist (ht)
  "Return alist corresponding to hash table HT"
  (let (alist)
    (maphash
     (lambda (k v)
       (push (cons k v) alist)
       )
     ht
     )
    alist
    ))

(defalias 'alist⟵ht 'ht/to-alist)


(cl-defun ht/to-plist (ht)
  "Return plist corresponding to hash table HT"
  (let (plist)
    (maphash
     (lambda (k v)
       (push v plist)
       (push k plist)
       )
     ht
     )
    plist
    ))

(defalias 'plist⟵ht 'ht/to-plist)


(cl-defun ht/⊉ (ht1 ht2 &key (valcmp #'equal))
  "Is hash table HT1 *not* a super set of hash table HT2?

In other words, does HT2 have any (key val) pair
not equally mirrored in HT1?

If so return the one of those keys.

Otherwise return nil.

Values are compared using VALCMP.

Note that keys in HT1 not contained in HT2 do not affect the outcome."
  (maphash
   (lambda (key ht2-val)
       (cond
        (ht2-val
         (let1  ht1-val (gethash key ht1)
           (unless (funcall valcmp ht1-val ht2-val)
             (cl-return-from ht/⊉ (or key t)
             )
           )))
        (t; ht2-val is nil
         (if (gethash key ht1 t)
             (cl-return-from ht/⊉ (or key t))
           ))
        ))
    ht2
    ))


(cl-defun ht/≠ (ht1 ht2 &key (valcmp #'equal))
  "Does hash table HT1 different in content from table HT2?

When different, the true value returned tries to give
give a small hint about the difference, perhaps helpful for debugging.

Return value defined procedurally:
If size HT2 ≠ size HT2 return size HT2.

Otherwise
Look through entries of HT2 looking for a key missing in HT1
or found but with a different value (compared using VALCMP).
Return (or key t) of the offending key.

Finally if no difference found, return nil.
"
  (if (/= (hash-table-count ht1) (hash-table-count ht2))
      (hash-table-count ht2)
    ;; Here we know ht1 and ht2 have the same number of entries.
    ;; So we only need to scan one of them so see to find a difference.
    (maphash
     (lambda (key ht2-val)
       (cond
        (ht2-val
         (let1  ht1-val (gethash key ht1)
           (unless (funcall valcmp ht1-val ht2-val)
             (cl-return-from ht/≠ (or key t)
             )
           )))
        (t; ht2-val is nil
         (if (gethash key ht1 t)
             (cl-return-from ht/≠ (or key t))
           ))
        ))
    ht2
    )))


(cl-defun ht/= (ht1 ht2 &key (valcmp #'equal))
  "Do hash tables HT1 and HT2 have the same contents?
Values compared with VALCMP"
  (not (ht/≠ ht1 ht2 :valcmp valcmp))
  )



(cl-defun ht/incr (ht key &key (start-val 1) (addend 1))
  "If KEY exists in hash table HT, increment its value
by adding ADDEND to it;
otherwise set the value to START-VAL.

Error if KEY has non-numeric value."
  (puthash
   key
   (if-let1  old-val  (gethash key ht)
       (+ addend old-val)
     start-val
     )
   ht
   ))


(defun ht/++ (ht key)
  "Like `ht/incr' called with default opts;
but may run faster."
  (puthash
   key
   (1+ (gethash key ht 0))
   ht
   ))





(provide 'ht-slash)

;;; ht-slash.el ends here
