;;; list-slash-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231102
;; Updated: 20240427
;; Version: 0.0
;; Keywords:

;;; Commentary:


;;; Change Log:

;;; Code:


(require 'let1)
(require 'list-slash)
(require 'plist-slash)


(ert-deftest list/singleton? ()
  (should
   (equal '(pig)
          (list/singleton? '(pig))
          ))
  (should
    (null
     (list/singleton? '(pig dog))
     )
    )
  (should
    (null
     (list/singleton? ())
     )
    )
   (should
    (null
     (list/singleton? [7])
     )
    )
   (should
    (null
     (list/singleton? 0)
     )
    )
   )


(ert-deftest list/len< ()
  (should
   (list/len< nil '(a))
   )
  (should
   (list/len< '(a) '(b c))
   )
  (should
   (not
    (list/len< nil nil)
    )
   )
  (should
   (not
    (list/len< '(a) nil)
    )
   )
  (should
   (not
    (list/len< '(a) '(a))
    ))
  (should
   (not
    (list/len< '(a b) nil)
    )
   )
  (should
   (not
    (list/len< '(a) nil)
    )
   ))


(ert-deftest list/len≦ ()
  (should
   (list/len≦ nil '(a))
   )
  (should
   (list/len≦ '(a) '(b c))
   )
  (should
   (list/len≦ nil nil)
   )
  (should
   (not
    (list/len≦ '(a) nil)
    ))
  (should
    (list/len≦ '(a) '(a))
    )
  (should
   (not
    (list/len≦ '(a b) nil)
    )
   )
  (should
   (not
    (list/len≦ '(a) nil)
    )
   ))


(ert-deftest list/prune ()
  (should
   (equal
    '(1 (3 (5)) 7 (9))
    (list/prune '(1 2 (3 (4 5) 6) 7 8 (9)) #'cl-evenp)
    )))


(ert-deftest list/before? ()
  (should
   (equal
    '(b c d)
    (list/before? '(a b c d) 'b 'd)
    ))
  (should
   (equal
    '(b c d)
    (list/before? '(a b c d) 'b 'z)
    )))


(ert-deftest list/before-after? ()
  (should
   (equal
    '(a d)
    (list/before-after? '(b a d) 'b 'a)
    ))
  (should
   (equal
    '(d)
    (list/before-after? '(b a d) 'b 'd)
    ))
  (should
   (null
    (list/before-after? '(b a d) 'a 'b)
    ))
  (should
   (null
    (list/before-after? '(a) 'a 'b)
    )))


(ert-deftest list/nth-occurrence ()
  (should
   (equal
    '(a b a b a b a c j a l)
    (list/nth-occurrence '(a b a b a b a c j a l) 'a 1)
    ))
  (should
   (equal
    '(a b a c j a l)
    (list/nth-occurrence '(a b a b a b a c j a l) 'a 3)
    ))
  (should
   (equal
    '(c)
    (list/nth-occurrence '(a b a b c b a c j a c) 'c 3)
    ))
  (should
   (null
    (list/nth-occurrence '(a b a b c b a c j a l) 'c 3)
    )))


(ert-deftest list/split-at ()
  (should
   (equal
    '((1 2 3 4)
      (5 6 7 8 9 10)
      )
    (list/split-at '(1 2 3 4 5 6 7 8 9 10)
                   #'(lambda (x) (> x 4))
                   )
    )))


(ert-deftest list/do-idx/forward-step1 ()
  (should
   (equal
    '((20 0) (30 1) (70 2))
    (let (acc)
      (list/do-idx* '(20 30 70) x (i) (reverse acc)
        (push (list x i) acc)
        )
      )
    )
   ))

(ert-deftest list/do-idx*/bakward-step1 ()
  (should
   (equal
    '((70 2) (30 1) (20 0))
    (let (acc)
      (list/do-idx* '(20 30 70) x (i) acc
        (push (list x i) acc)
        )
      )
    )
   ))


(ert-deftest list/do-idx*/forward-step2 ()
  (should
   (equal
    '(:a :c :d extra)
    (let (acc)
      (list/do-idx*
        '(:a apple :c cat :d dog extra)
        x
          (i 2)
        (reverse acc)
        (push x acc)
        ))
    ))
  (should
   (equal
    '(apple cat dog)
    (let (acc)
      (list/do-idx*
        '(:a apple :c cat :d dog extra)
        x
          (i 2 1)
        (reverse acc)
        (push x acc)
        ))
    ))
  )


(ert-deftest list/do-idx*/bakward-step2 ()
  (should
   (equal
    '(:a :c :d extra)
    (let (acc)
      (list/do-idx*
        '(:a apple :c cat :d dog extra)
        x
          (i -2)
        acc
        (push x acc)
        ))
    ))
  (should
   (equal
    '(apple cat dog)
    (let (
          (L '(:a apple :c cat :d dog extra))
          acc
          )
      (list/do-idx*
        L
        x
          (i -2  (- (length L) 2))
        acc
        (push x acc)
        ))
    ))
  )


(ert-deftest list/do-idx*/forward-modify ()
    (should
     (equal
      '(120 230 370)
     (let1 L (copy-sequence '(20 30 70));; copy-sequence necessary for test to work repeatedly.
        (list/do-idx* L x (i) t
          (setq x (+ (* 100 (1+ i)) x))
          )
        L
        )
      )))


(ert-deftest list/do-tween-idx/forward ()
  (should
   (t?
    (with-temp-buffer/string=
        "a0,b1,c2"
      (list/do-tween-idx
        '(?a ?b ?c)
        x
        (i)
        (insert ",")
        (insertf "%c%d" x i)
        ))
    )))



(ert-deftest list/do-tween-idx/bakward ()
  (should
   (t?
    (with-temp-buffer/string=
        "c2,b1,a0"
      (list/do-tween-idx
        '(?a ?b ?c)
        x
        (i -1)
          (insert ",")
        (insertf "%c%d" x i)
        )
      (buffer-string)
      )
    )))



(ert-deftest list/do-tween ()
  (should
   (t?
    (with-temp-buffer/string=
        "cat:dog:horse"
      (list/do-tween
        '("cat" "dog" "horse")
        s
        (insert ":")
        (insert s)
        )
      (buffer-string)
      )
    )))


(ert-deftest list/do-tween* ()
  (should
   (t?
    (with-temp-buffer/string=
        "cat:dog:horse"
      (list/do-tween*
        '("cat" "dog" "horse")
        s
          t
            (insert ":")
        (insert s)
        )
      (buffer-string)
      )
    )))



(ert-deftest list/do-idx/forward-step1/early-exit ()
  (should
   (equal
    '((20 0) (30 1))
    (let (acc)
      (list/do-idx* '(20 30 70) x (i) (reverse acc)
        (when (< 50 x) (cl-return))
        (push (list x i) acc)
        )
      )
    )
   ))


(ert-deftest list/item-count-ht ()
  (should
   (ht/=
    (plist/to-ht '(?a 3  ?b 1  ?d 4))
    (list/item-count-ht (append "ddddabaa" nil))
    ))
  (should
   (ht/=
    (plist/to-ht '(0 1  1 3  2 5  3 1))
    (list/item-count-ht
     '(0 1 2 3 4 5 6 7 8 9)
     :keyfun #'cl-isqrt
     )))
  )


(ert-deftest list/max-items-score ()
  (should
   (equal
    '(((a b c) (x y z)) 3)
    (list/max-items-score
     '((a b) (a b c) (g) (x y z))
     #'length
     )
    ))
  )


(ert-deftest list/max-item-score ()
  (should
   (equal
    '((a b c) 3)
    (list/max-item-score
     '((a b) (a b c) (g) (x y z))
     #'length
     )
    ))
  )


(ert-deftest list/first-item-val ()
  (should
   (equal
    '(3 9)
    (list/first-item-val
     '(2 3 4 5)
     (lambda (x) (and (cl-oddp x) (* x x)))
     ))))


(ert-deftest list/strip-extra-parens ()
  (should
   (equal '(a b c)
          (list/sans-extra-parens '(a b c))
          )
   )
  (should
   (equal '(a b c)
          (list/sans-extra-parens '((a b c)))
          )
   ))


(ert-deftest list/link/prev-linear ()
  (let1 L1 (list 'a 'b 'c)
    (should
     ;; The head link of L1 has no previous link.
     (eq nil
         (list/link/prev
          L1
          L1
          )))
    (should
     (eq (nthcdr 0 L1)
         (list/link/prev
          L1
          (nthcdr 1 L1)
          )))
    (should
     (eq (nthcdr 1 L1)
         (list/link/prev
          L1
          (nthcdr 2 L1)
          )))
    (should
     (eq nil
         (list/link/prev
          L1
          (cons 'b 'c);; `equal' perhaps, but not `eq' to any link in L1
          )))
    ))


(ert-deftest list/link/prev-circular ()
  (let1 L1 (list 'a 'b 'c)
    (setcdr (last L1) L1)
    (should
     ;; L1 circular, so the head link does have a previous link.
     (eq (nthcdr 2 L1)
         (list/link/prev
          L1
          L1
          )))
    (should
     (eq (nthcdr 0 L1)
         (list/link/prev
          L1
          (nthcdr 1 L1)
          )))
    (should
     (eq (nthcdr 1 L1)
         (list/link/prev
          L1
          (nthcdr 2 L1)
          )))
    (should
     (eq nil
         (list/link/prev
          L1
          (cons 'b 'c);; `equal' perhaps, but not `eq' to any link in L1
          )))
    ))


;;; list-slash-tests.el ends here
